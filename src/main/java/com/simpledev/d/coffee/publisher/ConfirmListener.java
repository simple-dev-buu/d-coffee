package com.simpledev.d.coffee.publisher;

public interface ConfirmListener<t> {

    public void onSetConfirm(t obj);
}
