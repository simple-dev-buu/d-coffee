package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.services.EmployeeServices;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PaymentDebt {

    private Employee emp;
    private String startDate;
    private double amount;

    public PaymentDebt(Employee emp, String startDate, double amount) {
        this.emp = emp;
        this.startDate = startDate;
        this.amount = amount;
    }

    public PaymentDebt(String startDate, double amount) {
        this.emp = null;
        this.startDate = startDate;
        this.amount = amount;
    }

    public PaymentDebt() {
        this.emp = null;
        this.startDate = "";
        this.amount = 0;
    }

    public Employee getEmployee() {
        return emp;
    }

    public void setEmployee(Employee emp) {
        this.emp = emp;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PaymentDebt{" + "emp=" + emp + ", startDate=" + startDate + ", amount=" + amount + '}';
    }

//    public double calculateSalary(Employee emp, CheckTime checkTime) {
//        double hourlyRate = emp.getMoneyRate();
//        int totalHoursWorked = checkTime.getTotalWorked();
//        int result = checkTime.getDate().compareTo(startDate);
//        
//        if(result>0) {
//            amount = hourlyRate * totalHoursWorked;
//        } else {
//            return amount;
//        }
//
//        return amount;
//    }

    public static PaymentDebt fromRS(ResultSet rs) {
        PaymentDebt paymentDebt = new PaymentDebt();
        EmployeeServices es = new EmployeeServices();
        try {
            int empId = rs.getInt("employee_id");
            paymentDebt.setEmployee(es.getById(empId));
            paymentDebt.setStartDate(rs.getString("paymentDebt_startDate"));
            paymentDebt.setAmount(rs.getDouble("paymentDebt_amount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return paymentDebt;
    }

}
