
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.SalaryPaymentDao;
import com.simpledev.d.coffee.models.SalaryPayment;
import java.util.ArrayList;
import java.util.List;

public class SalaryPaymentService {

    public ArrayList<SalaryPayment> getAll() {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return (ArrayList<SalaryPayment>) salaryPaymentDao.getAll();
    }

    public SalaryPayment get(int id) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.get(id);
    }

    public List<SalaryPayment> getSalaryPaymentsByOrder(String where, String order) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getAll(where, order);
    }

    public List<SalaryPayment> getSalaryPaymentsByOrderDESC() {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getAll("salaryPayment_id", "DESC");
    }

    public SalaryPayment addNew(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.insert(editedSalaryPayment);
    }

    public SalaryPayment update(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.update(editedSalaryPayment);
    }

    public int delete(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.delete(editedSalaryPayment);
    }

    public ArrayList<SalaryPayment> getByMonth(int month) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return salaryPaymentDao.getByMonth(month);
    }

    public ArrayList<SalaryPayment> getByStatus(String status) {
        SalaryPaymentDao salaryPaymentDao = new SalaryPaymentDao();
        return (ArrayList<SalaryPayment>) salaryPaymentDao.getByStatus(status);
    }

    public double getAllTotalPaid() {
        double expense = 0;
        ArrayList<SalaryPayment> salaryPayments = getAll();
        for (SalaryPayment s : salaryPayments) {
            expense += s.getTotalAmount();
        }
        return expense;
    }
}
