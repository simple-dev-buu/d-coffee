package com.simpledev.d.coffee.ui;

import com.simpledev.d.coffee.common.CommonDateTime;
import com.simpledev.d.coffee.ui.receipt.ReceiptRecords;
import com.simpledev.d.coffee.ui.invoice.InvoiceManagement;
import com.simpledev.d.coffee.ui.promotion.PromotionManagement;
import com.simpledev.d.coffee.ui.product.ProductManagement;
import com.simpledev.d.coffee.ui.customer.CustomerManagement;
import com.simpledev.d.coffee.ui.stock.StockManagement;
import com.simpledev.d.coffee.ui.pos.POS;
import com.simpledev.d.coffee.ui.employee.EmployeeManagement;
import com.simpledev.d.coffee.ui.stock.CheckStockRecords;
import com.simpledev.d.coffee.ui.check_time.CheckTimeRecords;
import com.simpledev.d.coffee.ui.dashboard.Dashboard;
import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.component.ProgressBarPanel;
import com.simpledev.d.coffee.component.RoundedButton;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.publisher.LoginCallback;
import com.simpledev.d.coffee.publisher.ProgressBarListener;
import com.simpledev.d.coffee.ui.bill.BillRecords;
import com.simpledev.d.coffee.ui.salary.DebtRecords;
import com.simpledev.d.coffee.ui.salary.SalaryRecords;
import com.simpledev.d.coffee.ui.user.UserManagement;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;
import java.util.concurrent.ExecutionException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.swing.Timer;

public class MainFrame extends javax.swing.JFrame implements LoginCallback, ProgressBarListener {

    private User user;
    private int role;
    private final int allBtnMenu;

    public MainFrame(User user, int role) {
        allBtnMenu = 12;
        this.user = user;
        this.role = role;
        initFrame();
        initComponents();
        initCustomComponents();
        initFrameOperator();
        repaintByUser();
        ShortcutCheckTime.addListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initCustomComponents() {
        initLogoImg();
        lbDateTime.setForeground(Color.white);
        mainView.setViewportView(new Dashboard());
        labelName.setFont(CommonFont.getFont());
        labelStatus.setFont(CommonFont.getFont());

        Timer timer = new Timer(1000, (ActionEvent e) -> {
            lbDateTime.setText("Date: " + CommonDateTime.getDateNowString() + " Time : " + CommonDateTime.getTimeNowString());
        });
        timer.start();

        progressBar.setBackground(new Color(0, 0, 0, 0));
        progressBar.setForeground(new Color(76, 135, 200));
        progressBar.setOpaque(true);

    }

    private void initFrameOperator() {
        panelTitle.setBackground(new Color(30, 13, 3));
        btnExit.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                btnExit.setText("x");
                btnHide.setText("-");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btnExit.setText("");
                btnHide.setText("");
            }

        });

        btnHide.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                setState(ICONIFIED);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                btnExit.setText("x");
                btnHide.setText("-");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                btnExit.setText("");
                btnHide.setText("");
            }
        });

    }

    private void initFrame() {
        final int width = 1366;
        final int height = 768;
        setPreferredSize(new Dimension(width, height));
        setUndecorated(true);
        setShape(new RoundRectangle2D.Double(0, 0, width, height, 20, 20));
        setResizable(false);

    }

    private void repaintByUser() {
        if (this.role == 1) {
            int rows = allBtnMenu - 6;
            JButton[] btns = {btnCustomer, btnEmp, btnSalary, btnOrder, btnUser, btnBill};
            for (JButton btn : btns) {
                panelMenu.remove(btn);
            }
            panelMenu.setLayout(new GridLayout(rows, 0, 0, 6));

        } else {
            panelMenu.setLayout(new GridLayout(allBtnMenu, 0, 0, 6));

        }
        panelMenu.revalidate();
        panelMenu.repaint();
        if (user != null) {
            btnLogin.setText("Logout");
            Employee emp = user.getEmployee();
            labelName.setText(emp.getFirstName() + " " + emp.getLastName());
            labelStatus.setText(emp.getTitle());
        } else {
            btnLogin.setText("Login");
            labelName.setText("");
            labelStatus.setText("");

        }
    }

    private void initLogoImg() {
        try {
            ImageIcon icon = new ImageIcon("assets/MainLogo.png");
            Image img = icon.getImage();
            Image imgScale = img.getScaledInstance(lbLogo.getWidth(), lbLogo.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon scaledIcon = new ImageIcon(imgScale);
            lbLogo.setIcon(scaledIcon);

            Image imgScaleTitle = img.getScaledInstance(lbLogoTitle.getWidth(), lbLogoTitle.getHeight(), Image.SCALE_SMOOTH);
            ImageIcon scaledIconTitle = new ImageIcon(imgScaleTitle);
            lbLogoTitle.setIcon(scaledIconTitle);
        } catch (Exception e) {
            System.out.println(e);
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTop = new javax.swing.JPanel();
        btnLogin = new RoundedButton("", new Color(249, 224, 187), Color.BLACK);
        lbMainMenu = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbUser = new javax.swing.JLabel();
        labelName = new javax.swing.JLabel();
        lbUser1 = new javax.swing.JLabel();
        labelStatus = new javax.swing.JLabel();
        mainView = new javax.swing.JScrollPane();
        panelTitle = new javax.swing.JPanel();
        btnExit = new RoundedButton("",new Color(255, 102, 102) , new Color(10,10,10));
        lbTitleBar = new javax.swing.JLabel();
        lbLogoTitle = new javax.swing.JLabel();
        btnHide = new RoundedButton("", new Color(255, 189, 68), new Color(10,10,10));
        lbDateTime = new javax.swing.JLabel();
        pnlProgressBar = new ProgressBarPanel();
        progressBar = new javax.swing.JProgressBar();
        panelMenuList = new javax.swing.JPanel();
        panelLogo = new javax.swing.JPanel();
        lbLogo = new javax.swing.JLabel();
        scrPanelMenu = new javax.swing.JScrollPane();
        panelMenu = new javax.swing.JPanel();
        btnMainMenu = new javax.swing.JButton();
        btnPos = new javax.swing.JButton();
        btnStock = new javax.swing.JButton();
        btnCustomer = new javax.swing.JButton();
        btnOrder = new javax.swing.JButton();
        btnSalary = new javax.swing.JButton();
        btnUser = new javax.swing.JButton();
        btnEmp = new javax.swing.JButton();
        btnProductMangement = new javax.swing.JButton();
        btnPromotionMangement = new javax.swing.JButton();
        btnCheckTime = new javax.swing.JButton();
        btnBill = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("D-Coffee");
        setBackground(new java.awt.Color(127, 69, 51));
        setUndecorated(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });

        panelTop.setBackground(new java.awt.Color(127, 69, 51));

        btnLogin.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        lbMainMenu.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbMainMenu.setForeground(new java.awt.Color(255, 255, 255));
        lbMainMenu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbMainMenu.setText("Dashboard");

        jPanel1.setBackground(new java.awt.Color(127, 69, 51));

        lbUser.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbUser.setForeground(new java.awt.Color(255, 255, 255));
        lbUser.setText("User :");

        labelName.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        labelName.setForeground(new java.awt.Color(255, 255, 255));
        labelName.setText("--");

        lbUser1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbUser1.setForeground(new java.awt.Color(255, 255, 255));
        lbUser1.setText("Status :");

        labelStatus.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N
        labelStatus.setForeground(new java.awt.Color(255, 255, 255));
        labelStatus.setText("--");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbUser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelName, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(138, 138, 138))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbUser)
                    .addComponent(labelName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbUser1)
                    .addComponent(labelStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout panelTopLayout = new javax.swing.GroupLayout(panelTop);
        panelTop.setLayout(panelTopLayout);
        panelTopLayout.setHorizontalGroup(
            panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTopLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
        panelTopLayout.setVerticalGroup(
            panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTopLayout.createSequentialGroup()
                .addGroup(panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelTopLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelTopLayout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panelTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        mainView.setBackground(new Color(0,0,0,0));
        mainView.setBorder(null);
        mainView.setViewportView(null);

        panelTitle.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        panelTitle.add(btnExit, new org.netbeans.lib.awtextra.AbsoluteConstraints(1330, 10, 21, 20));

        lbTitleBar.setFont(new java.awt.Font("Helvetica Neue", 1, 13)); // NOI18N
        lbTitleBar.setForeground(new java.awt.Color(255, 255, 255));
        lbTitleBar.setText("D-Coffee");
        panelTitle.add(lbTitleBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 10, -1, -1));

        lbLogoTitle.setBackground(new java.awt.Color(255, 204, 255));
        lbLogoTitle.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbLogoTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbLogoTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelTitle.add(lbLogoTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 20, 20));

        btnHide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHideActionPerformed(evt);
            }
        });
        panelTitle.add(btnHide, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 10, 21, 20));

        lbDateTime.setText("Date Time");
        panelTitle.add(lbDateTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 10, 290, -1));

        pnlProgressBar.setLayout(new java.awt.GridLayout(1, 0));
        pnlProgressBar.add(progressBar);

        panelTitle.add(pnlProgressBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 8, 750, 20));

        panelMenuList.setBackground(new java.awt.Color(162, 108, 62));

        panelLogo.setOpaque(false);

        lbLogo.setBackground(new java.awt.Color(255, 204, 204));
        lbLogo.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbLogo.setForeground(new java.awt.Color(255, 255, 255));
        lbLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout panelLogoLayout = new javax.swing.GroupLayout(panelLogo);
        panelLogo.setLayout(panelLogoLayout);
        panelLogoLayout.setHorizontalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLogoLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );
        panelLogoLayout.setVerticalGroup(
            panelLogoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLogoLayout.createSequentialGroup()
                .addContainerGap(9, Short.MAX_VALUE)
                .addComponent(lbLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        scrPanelMenu.setBorder(null);

        panelMenu.setBackground(new Color(162, 108, 62));
        panelMenu.setMinimumSize(new java.awt.Dimension(194, 637));
        panelMenu.setLayout(new java.awt.GridLayout(12, 0, 0, 6));

        btnMainMenu.setBackground(new java.awt.Color(249, 224, 187));
        btnMainMenu.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnMainMenu.setText("Main Menu");
        btnMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainMenuActionPerformed(evt);
            }
        });
        panelMenu.add(btnMainMenu);

        btnPos.setBackground(new java.awt.Color(249, 224, 187));
        btnPos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPos.setText("POS");
        btnPos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPosActionPerformed(evt);
            }
        });
        panelMenu.add(btnPos);

        btnStock.setBackground(new java.awt.Color(249, 224, 187));
        btnStock.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnStock.setText("Stock");
        btnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockActionPerformed(evt);
            }
        });
        panelMenu.add(btnStock);

        btnCustomer.setBackground(new java.awt.Color(249, 224, 187));
        btnCustomer.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnCustomer.setText("Customers");
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });
        panelMenu.add(btnCustomer);

        btnOrder.setBackground(new java.awt.Color(249, 224, 187));
        btnOrder.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnOrder.setText("Invoices");
        btnOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOrderActionPerformed(evt);
            }
        });
        panelMenu.add(btnOrder);

        btnSalary.setBackground(new java.awt.Color(249, 224, 187));
        btnSalary.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnSalary.setText("Salaries");
        btnSalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalaryActionPerformed(evt);
            }
        });
        panelMenu.add(btnSalary);

        btnUser.setBackground(new java.awt.Color(249, 224, 187));
        btnUser.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnUser.setText("Users");
        btnUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUserActionPerformed(evt);
            }
        });
        panelMenu.add(btnUser);

        btnEmp.setBackground(new java.awt.Color(249, 224, 187));
        btnEmp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnEmp.setText("Employees");
        btnEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmpActionPerformed(evt);
            }
        });
        panelMenu.add(btnEmp);

        btnProductMangement.setBackground(new java.awt.Color(249, 224, 187));
        btnProductMangement.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnProductMangement.setText("Products");
        btnProductMangement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProductMangementActionPerformed(evt);
            }
        });
        panelMenu.add(btnProductMangement);

        btnPromotionMangement.setBackground(new java.awt.Color(249, 224, 187));
        btnPromotionMangement.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPromotionMangement.setText("Promotion");
        btnPromotionMangement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionMangementActionPerformed(evt);
            }
        });
        panelMenu.add(btnPromotionMangement);

        btnCheckTime.setBackground(new java.awt.Color(249, 224, 187));
        btnCheckTime.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCheckTime.setText("Check Time");
        btnCheckTime.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckTimeActionPerformed(evt);
            }
        });
        panelMenu.add(btnCheckTime);

        btnBill.setBackground(new java.awt.Color(249, 224, 187));
        btnBill.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnBill.setText("Bills");
        btnBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBillActionPerformed(evt);
            }
        });
        panelMenu.add(btnBill);

        scrPanelMenu.setViewportView(panelMenu);

        javax.swing.GroupLayout panelMenuListLayout = new javax.swing.GroupLayout(panelMenuList);
        panelMenuList.setLayout(panelMenuListLayout);
        panelMenuListLayout.setHorizontalGroup(
            panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuListLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scrPanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );
        panelMenuListLayout.setVerticalGroup(
            panelMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuListLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrPanelMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 602, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 1360, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelMenuList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(0, 0, 0)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mainView, javax.swing.GroupLayout.DEFAULT_SIZE, 1163, Short.MAX_VALUE)
                            .addComponent(panelTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(panelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(mainView))
                    .addComponent(panelMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
        setLocation(evt.getXOnScreen() - x, evt.getYOnScreen() - y);
    }//GEN-LAST:event_formMouseDragged

    private int x;
    private int y;
    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        x = evt.getX();
        y = evt.getY();
    }//GEN-LAST:event_formMousePressed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnHideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHideActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHideActionPerformed

    private void btnPromotionMangementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromotionMangementActionPerformed
        mainView.setViewportView(new PromotionManagement());
        lbMainMenu.setText("Promotion Management");
    }//GEN-LAST:event_btnPromotionMangementActionPerformed

    private void btnBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBillActionPerformed
        mainView.setViewportView(new BillRecords());
        lbMainMenu.setText("Bill Records");
    }//GEN-LAST:event_btnBillActionPerformed

    private void btnPosActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnPosActionPerformed
        mainView.setViewportView(new POS(user));
        lbMainMenu.setText("Point of Sale");
    }// GEN-LAST:event_btnPosActionPerformed

    private void btnMainMenuActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnMainMenuActionPerformed
        mainView.setViewportView(new Dashboard());
        lbMainMenu.setText("Dashboard");

    }

    private void btnStockActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnStockActionPerformed

        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(new JMenuItem("Ingredient Management")).addActionListener(e -> {
            mainView.setViewportView(new StockManagement(user));
            lbMainMenu.setText("Ingredient Management");
        });
        popupMenu.add(new JMenuItem("Check Stock Records")).addActionListener(e -> {
            mainView.setViewportView(new CheckStockRecords());
            lbMainMenu.setText("Check Stock Records");
        });
        popupMenu.add(new JMenuItem("Receipt Records")).addActionListener(e -> {
            mainView.setViewportView(new ReceiptRecords());
            lbMainMenu.setText("Receipt Records");
        });

        popupMenu.show(btnStock, btnStock.getX(), y);
    }// GEN-LAST:event_btnStockActionPerformed

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCustomerActionPerformed
        mainView.setViewportView(new CustomerManagement());
        lbMainMenu.setText("Customer Management");
    }// GEN-LAST:event_btnCustomerActionPerformed

    private void btnOrderActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnOrderActionPerformed
        mainView.setViewportView(new InvoiceManagement());
        lbMainMenu.setText("Invoice Records");
    }// GEN-LAST:event_btnOrderActionPerformed

    private void btnCheckTimeActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCheckTimeActionPerformed
        mainView.setViewportView(new CheckTimeRecords(user));
        lbMainMenu.setText("Check Time Records");
    }// GEN-LAST:event_btnCheckTimeActionPerformed

    private void btnSalaryActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSalaryActionPerformed

        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(new JMenuItem("Records")).addActionListener(e -> {
            mainView.setViewportView(new SalaryRecords());
            lbMainMenu.setText("Salary Records");
        });
        popupMenu.add(new JMenuItem("Debt Payments")).addActionListener(e -> {
            mainView.setViewportView(new DebtRecords(user));
            lbMainMenu.setText("Debt payment");
        });

        popupMenu.show(btnSalary, btnSalary.getX(), y);

    }// GEN-LAST:event_btnSalaryActionPerformed

    private void btnUserActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnUserActionPerformed
        mainView.setViewportView(new UserManagement());
        lbMainMenu.setText("User Management");
    }// GEN-LAST:event_btnUserActionPerformed

    private void btnEmpActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnEmpActionPerformed
        mainView.setViewportView(new EmployeeManagement());
        lbMainMenu.setText("Employee Management");
    }

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnLoginActionPerformed
        if (user != null) {
            int confirm = JOptionPane.showConfirmDialog(this, "Are you sure to logout ?", "Confirm Logout",
                    JOptionPane.WARNING_MESSAGE);
            if (confirm == 0) {
                newLoginFrame();
                this.dispose();
            }
        } else {
            newLoginFrame();
        }
    }

    private void newLoginFrame() {
        Login loginFrame = new Login();
        // set center of screen
        loginFrame.setLocationRelativeTo(this);
        loginFrame.setVisible(true);
        loginFrame.requestFocus();
    }

    private void btnProductMangementActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnProductMangementActionPerformed
        mainView.setViewportView(new ProductManagement());
        lbMainMenu.setText("Product Management");
    }// GEN-LAST:event_btnProductMangementActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBill;
    private javax.swing.JButton btnCheckTime;
    private javax.swing.JButton btnCustomer;
    private javax.swing.JButton btnEmp;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnHide;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnMainMenu;
    private javax.swing.JButton btnOrder;
    private javax.swing.JButton btnPos;
    private javax.swing.JButton btnProductMangement;
    private javax.swing.JButton btnPromotionMangement;
    private javax.swing.JButton btnSalary;
    private javax.swing.JButton btnStock;
    private javax.swing.JButton btnUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelStatus;
    private javax.swing.JLabel lbDateTime;
    private javax.swing.JLabel lbLogo;
    private javax.swing.JLabel lbLogoTitle;
    private javax.swing.JLabel lbMainMenu;
    private javax.swing.JLabel lbTitleBar;
    private javax.swing.JLabel lbUser;
    private javax.swing.JLabel lbUser1;
    private javax.swing.JScrollPane mainView;
    private javax.swing.JPanel panelLogo;
    private javax.swing.JPanel panelMenu;
    private javax.swing.JPanel panelMenuList;
    private javax.swing.JPanel panelTitle;
    private javax.swing.JPanel panelTop;
    private javax.swing.JPanel pnlProgressBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JScrollPane scrPanelMenu;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onLoginSuccess(User user, int role) {
        this.user = user;
        this.role = role;
        repaintByUser();
        ShortcutCheckTime sct = new ShortcutCheckTime(user);
        sct.setLocationRelativeTo(this);
        sct.setVisible(true);
    }

    @Override
    public void onCompletedLoad() {
        pnlProgressBar.setVisible(true);
        loadingAnimate(progressBar);
    }

    private void loadingAnimate(JProgressBar progressBar) {
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                // Simulate a time-consuming task
                for (int i = 0; i <= 100; i++) {
                    Thread.sleep(3);
                    setProgress(i);
                }
                return null;
            }

            @Override
            protected void done() {
                // This method is called when the SwingWorker is done
                try {
                    get(); // Retrieve any result, even if it's Void
                } catch (InterruptedException | ExecutionException e) {
                }

            }

        };
        worker.addPropertyChangeListener(e -> {
            if ("progress".equals(e.getPropertyName())) {
                int progress = (Integer) e.getNewValue();
                progressBar.setValue(progress);
                if (progress == 100) {
                    pnlProgressBar.setVisible(false);
                    progressBar.setValue(0);
                }
            }

        });
        worker.execute();

    }
}
