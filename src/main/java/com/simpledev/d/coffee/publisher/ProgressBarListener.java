
package com.simpledev.d.coffee.publisher;


public interface ProgressBarListener {
    void onCompletedLoad();
}
