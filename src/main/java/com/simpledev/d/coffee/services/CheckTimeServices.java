package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CheckTimeDao;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.Employee;
import java.util.ArrayList;
import java.util.List;

public class CheckTimeServices {

    public ArrayList<CheckTime> getAll() {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return (ArrayList<CheckTime>) checkTimeDao.getAll();
    }
    
    public CheckTime getByEmpId(int id) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getByEmpId(id);
    }

    public List<CheckTime> getCheckInOutByOrder(String where, String order) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getAll(where, order);
    }

    public List<CheckTime> getCheckInOutByOrderDESC() {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getAll("checkTime_id", "DESC");
    }

    public void addNew(CheckTime time) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.insert(time);
    }

    public void update(CheckTime time) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.update(time);
    }

    public void updateCheckOut(CheckTime time) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.updateCheckOut(time);
    }

    public void delete(CheckTime time) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.delete(time);
    }

    public CheckTime getForCheckOut(int id, String date) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getForCheckOut(id, date);
    }
}
