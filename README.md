## D-Coffee by Simple Dev.

โปรแกรมนี้ เป็นโปรแกรมของร้านกาแฟ D-Coffee มี Features หลักต่าง ๆ พร้อมวิธีใช้งาน ดังนี้
- เริ่มเข้าโปรแกรม
	- กด run ที่ MainApp จะเห็นหน้าต่าง Login ให้ท่านกรอก user กับ password ให้ถูกต้อง
	- หากท่านเป็น admin ท่านจะทุกเมนู แต่หากท่านเป็นพนักงาน ท่านจะเห็นแค่บางเมนู
- วิธีจัดการ Stock
	- กรณีต้องการอัพเดทวัตถุดิบในคลัง
		- กดปุุ่มเมนู Stock เลือก Ingredient... จากนั้นเลือก Check Stock ทำการกรอกข้อมูลแล้วกด save
	- กรณีต้องการเพิ่มวัตถุดิบที่ไปซื้อมา
		- กดปุ่มเมนู Stock เลือก Ingredient... จากนั้นเลือก Make Receipt ทำการกรอกข้อมูลแล้วกด save
	- กรณีต้องการดูบันทึกของการ check stock
		- กดปุ่มเมนู Stock เลือก Check Stock Records
	- กรณีต้องการดูบันทึกใบเสร็จที่ซื้อของเข้า stock
		- กดปุ่มเมนู Stock เลือก Receipt Records
- วิธีดูรายงาน
	- สามารถดูรายงานการเงินได้ที่ Main Menu โดยปกติ ท่านจะเป็นหน้าแรกอยู่แล้ว
- วิธีขายของ
	1. เลือกสินค้าที่ต้องการจาก Product List (หลักจากกดเลือกสินค้าที่ต้องการแล้วจะแสดงรายการที่เลือกบน Item List)
		- สามารถเลือกประเภทสินค้า คือ Drink, Bakery
		- สามารถเพิ่ม ลด จำนวณสินค้าที่ต้องการ
	2. ตรวจสอบสมาชิก
		- กดปุ่ม Member Search เพื่อตรวจสอบสมาชิก 1 ครั้งเพื่อกรอกเบอร์มือถือสมาชิก และกดอีกครั้งเพื่อทำการค้นหาสมาชิก
		- หากเป็นสมาชิกสามารถใช้ Promotion ได้ โดยการกดปุ่ม Promotion เพื่อเลือก Promotion ที่ต้องการ เพื่อทำการใช้ส่วนลด และกด Calculate เพื่อทำการคำนวณส่วนลด
		- หากไม่เป็นสมาชิกแล้วต้องการสมัครสมาชิกให้กดปุ่ม Register 
	3. เลือกวิธีชำระเงิน
	4. ใส่จำนวนเงิน
		- ถ้าไม่ได้ใส่จำนวนเงินหรือใส่ไม่ครบ จะขึ้นแจ้งเตือน
	5. กดปุ่ม Calculate เพื่อคำนวณจำนวนเงินที่ต้องจ่าย
	6. กดปุ่ม Confirm เพื่อแสดงใบเสร็จ (ปุ่มสามารถกดได้เมื่อกด Calculate หรือคำนวณรายการทั้งหมดเสร็จแล้ว)
	7. เช็คใบเสร็จการซื้อ เมื่อต้องการยืนยันการชำระเงินให้กดปุ่ม Confirm หรือ กดปุ่ม Cancel เพื่อยกเลิกคำสั่งซื้อ
- วิธีเช็คอินและเช็คเอ้าท์
	1. เมื่อ Login จะมีหน้าต่างเช็คเวลาเด้งขึ้นโดย เวลาล็อคอินจะล็อค ณ เวลาที่ได้ทำการล็อคอินเข้ามา 
	2. เลือก Check In ค่าทั้งหมดจะถูกดึงลงมา ในหน้า CheckTime ยกเว้น Checkout
	3. หากพนักงานคนอื่นต้องการ Check In ให้ Logout ออก
	4. หากยังไม่ต้องการ Check In สามารถกด Later เมื่อปิดต่างต่าง
	5. สามารถ Check In ภายหลังได้โดยกด btnCheckIn/Out ในหน้า CheckTime หน้าต่างเช็คจะเด้งขึ้น
	6. เมื่อถึงเวลาเลิกงานเลือก ปุ่ม btnCheckIn/Out กด Check Out ค่าเวลาจะถูกดึงลงมาใน Checkout
- วิธีใช้งานระบบจ่ายเงินพนักงานและดูบันทึกการจ่ายเงิน
	1. จ่ายเงินพนักงาน 
		- คลิกซ้ายที่แถบเมนู Salaries เลือก Debt payment
		- กดเลือกพนักงานคนที่ต้องการจะจ่ายเงินที่ค้างอยู่ จากนั้นกดปุ่ม Pay ทางด้านขวาบนของหน้าจอ
		- จะขึ้นหน้ายืนยันสลิปการจ่ายเงิน Salary Slip กดปุ่ม Proceed ด้านซ้ายล่างเพื่อยืนยันการจ่ายเงินเดือน
		- หลังจากกด Proceed จะทำการบันทึกข้อมูลสลิปนั้นไปยังหน้า Salary Records และลบข้อมูล Debt payment นั้นออกไป
	2. ดูบันทึกการจ่ายเงินพนักงาน
		- คลิกซ้ายที่แถบเมนู Salaries เลือก Rocords
		- มีกล่องข้อความสำหรับค้นหาข้างบน ซึ่งสามารถเลือกที่จะค้นหาด้วย ID  , Employee Id และ วันที่ ด้วยกล่องตัวเลือกทางด้านขวาของช่อง search
		- มีปุ่มตัวเลือกให้แสดงรายการทั้งหมดทุกเดือน หรือแสดงตามเดือนใดเดือนหนึ่งที่เลือก ทางด้านล่างช่อง search
		- หากต้องการลบบันทึกการจ่ายเงิน สามารถกดเลือกรายการใดรายการหนึ่งที่ต้องการลบ แล้วกดปุ่ม Delete ทางด้านขวาบน เพื่อลบรายการนั้น