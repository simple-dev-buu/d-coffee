package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.services.EmployeeServices;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SalaryPayment {

    private int id;
    private Employee emp;
    private String date;
    private String type;
    private double totalAmount;
    private String status;

    public SalaryPayment(int id, Employee emp, String date, String type, double totalAmount, String status) {
        this.id = id;
        this.emp = emp;
        this.date = date;
        this.type = type;
        this.totalAmount = totalAmount;
        this.status = status;
    }
    
    public SalaryPayment(Employee emp, String date, String type, double totalAmount, String status) {
        this.id = -1;
        this.emp = emp;
        this.date = date;
        this.type = type;
        this.totalAmount = totalAmount;
        this.status = status;
    }
    
     public SalaryPayment() {
        this.id = -1;
        this.emp = new Employee();
        this.date = null;
        this.type = "";
        this.totalAmount = 0;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SalaryPayment{" + "id=" + id + ", emp=" + emp + ", date=" + date + ", type=" + type + ", totalAmount=" + totalAmount + ", status=" + status + '}';
    }
    
    public static SalaryPayment fromRS(ResultSet rs) {
        SalaryPayment salarySlip = new SalaryPayment();
        EmployeeServices empS = new EmployeeServices();
        int emp_Id;
        try {
            emp_Id = (rs.getInt("employee_id"));
            
            salarySlip.setId(rs.getInt("salaryPayment_id"));
            salarySlip.setEmp(empS.getById(emp_Id));
            salarySlip.setDate(rs.getString("salaryPayment_date"));
            salarySlip.setType(rs.getString("salaryPayment_type"));
            salarySlip.setTotalAmount(rs.getDouble("salaryPayment_totalAmount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salarySlip;
    }
    
}
