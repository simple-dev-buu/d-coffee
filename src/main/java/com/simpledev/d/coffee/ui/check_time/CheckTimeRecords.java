package com.simpledev.d.coffee.ui.check_time;

import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.CheckTimeServices;
import com.simpledev.d.coffee.ui.ShortcutCheckTime;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class CheckTimeRecords extends javax.swing.JPanel {

    private ArrayList<CheckTime> listCheckTime;
    private final CheckTimeServices checkTimeServices;
    private SearchSystem<CheckTime> searchSystem;
    private Predicate<CheckTime> filterFunction;
    private String searchValue;
    private int searchValueInt;
    private final User user;

    public CheckTimeRecords(User user) {
        initComponents();
        checkTimeServices = new CheckTimeServices();
        listCheckTime = checkTimeServices.getAll();
        initTables();
        initSearchSystem();
        this.user = user;
    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            
            case 0 -> {
                // ID
                filterFunction = ch -> ch.getId() == searchValueInt;
            }
            case 1 -> {
                // Date
                filterFunction = ch -> ch.getDate().contains(searchValue);
            }
            case 2 -> {
                // EMP name
                filterFunction = ch -> (ch.getEmployee().getFirstName() + ch.getEmployee().getLastName()).toLowerCase().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle chalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for chalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, listCheckTime);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = {"Id", "Date", "Emp Name"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tableCheckTimeList.setModel(new AbstractTableModel() {
            String[] headers = {
                "Checktime id",
                "Date",
                "Employee Name",
                "Checkin",
                "Checkout",
                "Total Worked",
                "Type"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return listCheckTime.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckTime che = listCheckTime.get(rowIndex);

                return switch (columnIndex) {
                    case 0 -> che.getId();
                    case 1 -> che.getDate();
                    case 2 -> che.getEmployee().getFirstName()+" "+che.getEmployee().getLastName();
                    case 3 -> che.getCheckIn();
                    case 4 -> che.getCheckOut();
                    case 5 -> che.getTotalWorked();
                    case 6 -> che.getType();
                    default -> "unknow";
                };
            }

        });
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        checkTimeHead = new javax.swing.JPanel();
        searchBar = new javax.swing.JTextField();
        searchFilter = new javax.swing.JComboBox<>();
        btnCheckInOut = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCheckTimeList = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 204, 204));

        checkTimeHead.setBackground(new java.awt.Color(255, 153, 153));

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnCheckInOut.setText("Check In/Out");
        btnCheckInOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckInOutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout checkTimeHeadLayout = new javax.swing.GroupLayout(checkTimeHead);
        checkTimeHead.setLayout(checkTimeHeadLayout);
        checkTimeHeadLayout.setHorizontalGroup(
            checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, checkTimeHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCheckInOut)
                .addContainerGap())
        );
        checkTimeHeadLayout.setVerticalGroup(
            checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(checkTimeHeadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(checkTimeHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCheckInOut, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(tableCheckTimeList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(checkTimeHead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(checkTimeHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckInOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckInOutActionPerformed
        ShortcutCheckTime sct = new ShortcutCheckTime(this.user);
        sct.setLocationRelativeTo(this);
        sct.setVisible(true);
        sct.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }
            
        });
    }//GEN-LAST:event_btnCheckInOutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckInOut;
    private javax.swing.JPanel checkTimeHead;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tableCheckTimeList;
    // End of variables declaration//GEN-END:variables

    private void reloadTables() {
        listCheckTime = searchSystem.getResults();
        tableCheckTimeList.validate();
        tableCheckTimeList.repaint();
    }

    private void refreshTables() {
        listCheckTime = checkTimeServices.getAll();
        tableCheckTimeList.revalidate();
        tableCheckTimeList.repaint();
    }



}
