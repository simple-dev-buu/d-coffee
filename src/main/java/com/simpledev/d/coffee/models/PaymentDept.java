package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PaymentDept {

    private int empId;
    private String startDate;
    private double amount;

    public PaymentDept() {
        this.empId = -1;
        this.startDate = "";
        this.amount = 0;
    }

    public PaymentDept(int empId, String startDate, double amount) {
        this.empId = empId;
        this.startDate = startDate;
        this.amount = amount;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PaymentDept{" + "empId=" + empId + ", startDate=" + startDate + ", amount=" + amount + '}';
    }

    public static PaymentDept fromRS(ResultSet rs) {
        PaymentDept pd = new PaymentDept();
        try {
            pd.setEmpId(rs.getInt("employee_id"));
            pd.setStartDate(rs.getString("paymentDept_Date"));
            pd.setAmount(rs.getInt("paymentDept_Amount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return pd;
    }

}
