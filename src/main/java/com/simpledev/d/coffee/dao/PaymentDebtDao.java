
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.PaymentDebt;
import com.simpledev.d.coffee.models.PaymentDebt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class PaymentDebtDao implements Dao<PaymentDebt>{

    @Override
    public PaymentDebt get(int id) {
      PaymentDebt paymentDebt = new PaymentDebt();
        String sql = "SELECT * FROM PAYMENT_DEBT WHERE employee_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                paymentDebt = PaymentDebt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return paymentDebt;
    }

    @Override
    public List<PaymentDebt> getAll() {
        PaymentDebt paymentDebt = new PaymentDebt();
        ArrayList<PaymentDebt> list = new ArrayList();
        String sql = "SELECT * FROM PAYMENT_DEBT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                paymentDebt = PaymentDebt.fromRS(rs);
                list.add(paymentDebt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public ArrayList<PaymentDebt> getByMonth(int month) {
            String formattedMonth = String.format("%02d", month);
            PaymentDebt paymentDebt;
            ArrayList<PaymentDebt> list = new ArrayList();
            String sql = "SELECT * FROM PAYMENT_DEBT WHERE strftime('%m',paymentDebt_startDate)=?";
            Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, formattedMonth);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                paymentDebt = PaymentDebt.fromRS(rs);
                list.add(paymentDebt);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public PaymentDebt insert(PaymentDebt obj) {
        String sql = "INSERT INTO PAYMENT_DEBT (employee_id, paymentDebt_startDate, paymentDebt_amount)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2,obj.getStartDate());
            stmt.setDouble(3, obj.getAmount());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }


    @Override
    public PaymentDebt update(PaymentDebt obj) {
        String sql = "UPDATE PAYMENT_DEBT"
                + " SET paymentDebt_startDate=?, paymentDebt_amount = ?"
                + " WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getStartDate());
            stmt.setDouble(2, obj.getAmount());
            stmt.setInt(3, obj.getEmployee().getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }}

    @Override
    public int delete(PaymentDebt obj) {
        String sql = "DELETE FROM PAYMENT_DEBT WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        }

    @Override
    public List<PaymentDebt> getAll(String where, String order) {

        ArrayList<PaymentDebt> list = new ArrayList<PaymentDebt>();
        String sql = "SELECT * FROM PAYMENT_DEBT ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PaymentDebt paymentDebt = PaymentDebt.fromRS(rs);
                list.add(paymentDebt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
