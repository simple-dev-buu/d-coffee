package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.BillDao;
import com.simpledev.d.coffee.models.Bill;
import java.util.ArrayList;

public class BillService {

    public ArrayList<Bill> getAll() {
        return (ArrayList<Bill>) new BillDao().getAll();
    }

    public Bill addNew(Bill obj) {
        return new BillDao().insert(obj);
    }

    public Bill update(Bill obj) {
        return new BillDao().update(obj);
    }

    public int delete(Bill obj) {
        return new BillDao().delete(obj);
    }

    public Bill getById(int id) {
        return new BillDao().get(id);
    }

    public double getAllTotalExpense() {
        double expense = 0;
        ArrayList<Bill> bills = getAll();
        for (Bill b : bills) {
            expense += b.getAmount();
        }
        return expense;
    }
}
