/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.ReceiptDao;
import com.simpledev.d.coffee.dao.IngredientDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class ReceiptDetail {

    private int id;
    private Receipt receipt;
    private Ingredient ingredient;
    private int quantity;
    private String description;
    private float pricePerUnit;
    private float discount;
    private float netPrice;

    public ReceiptDetail(int id, Receipt receiptid, Ingredient ingrediantcode, int quantity, String description, float pricePerUnit, float discount, float netPrice) {
        this.id = -1;
        this.receipt = receiptid;
        this.ingredient = ingrediantcode;
        this.quantity = quantity;
        this.description = description;
        this.pricePerUnit = pricePerUnit;
        this.discount = discount;
        this.netPrice = netPrice;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.receipt = null;
        this.ingredient = null;
        this.quantity = 0;
        this.description = "";
        this.pricePerUnit = 0;
        this.discount = 0;
        this.netPrice = 0;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Ingredient getIngrediant() {
        return ingredient;
    }

    public void setIngrediant(Ingredient ingrediant) {
        this.ingredient = ingrediant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", receiptid=" + receipt + ", ingrediantcode=" + ingredient + ", quantity=" + quantity + ", description=" + description + ", pricePerUnit=" + pricePerUnit + ", discount=" + discount + ", netPrice=" + netPrice + '}';
    }

    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        ReceiptDao receiptDao = new ReceiptDao();
        IngredientDao ingredientDao = new IngredientDao();
        int receipt_id;
        int ind_id;
        try {
            // get id
            receipt_id = (rs.getInt("receipt_id"));
            ind_id = (rs.getInt("ingredient_id"));
            // set object
            receiptDetail.setId(rs.getInt("receiptDetail_id"));
            receiptDetail.setQuantity(rs.getInt("receiptDetail_quantity"));
            receiptDetail.setDescription(rs.getString("receiptDetail_description"));
            receiptDetail.setPricePerUnit(rs.getFloat("receiptDetail_pricePerUnit"));
            receiptDetail.setDiscount(rs.getFloat("receiptDetail_discount"));
            receiptDetail.setNetPrice(rs.getFloat("receiptDetail_netPrice"));
            receiptDetail.setReceipt(receiptDao.get(receipt_id));
            receiptDetail.setIngrediant(ingredientDao.get(ind_id));

        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }
}
