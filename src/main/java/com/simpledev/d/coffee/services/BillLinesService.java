package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.BillLineDao;
import com.simpledev.d.coffee.models.Bill;
import com.simpledev.d.coffee.models.BillLine;
import java.util.ArrayList;

public class BillLinesService {

    public ArrayList<BillLine> getAll() {
        return (ArrayList<BillLine>) new BillLineDao().getAll();
    }

    public BillLine addNew(BillLine obj) {
        return new BillLineDao().insert(obj);
    }

    public BillLine update(BillLine obj) {
        return new BillLineDao().update(obj);
    }

    public int delete(BillLine obj) {
        return new BillLineDao().delete(obj);
    }

    public BillLine getById(int id) {
        return new BillLineDao().get(id);
    }

    public ArrayList<BillLine> getAllOf(Bill bill) {
        return (ArrayList<BillLine>) new BillLineDao().getAllOf(bill);
    }
}
