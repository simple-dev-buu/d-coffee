package com.simpledev.d.coffee.ui.stock;

import com.simpledev.d.coffee.common.CommonDateTime;
import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.models.CheckStock;
import com.simpledev.d.coffee.models.Ingredient;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.CheckStockServices;
import com.simpledev.d.coffee.services.IngredientService;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

public class UpdateStockDialog extends javax.swing.JDialog {

    private final CheckStockServices checkStockServices;
    private final IngredientService ingredientService;
    private Ingredient editIngredient;
    private final ArrayList<Ingredient> oldListIngredients;
    private ArrayList<Ingredient> newListIngredients;
    private int totalUnitLoss;
    private User user;

    public UpdateStockDialog(Frame parent, ArrayList<Ingredient> listIngredients, User user) {
        super(parent, true);
        this.user = user;
        oldListIngredients = listIngredients;
        newListIngredients = new ArrayList<>(oldListIngredients);
        this.checkStockServices = new CheckStockServices();
        this.ingredientService = new IngredientService();
        // this.checkStockDetailServices = new CheckStockDetailServices();

        initComponents();
        initOldTables();
        initNewTables();
        // ingredientService = new IngredientService();
        // listIngredients = ingredientService.getIngredientOrderById();
        // int IngredientSize = listIngredients.size();
        // for(int i=0;i<IngredientSize;i++){
        // pnlStockList.add(new EditPanel());
        // }
        // pnlStockList.setLayout(new
        // GridLayout((IngredientSize/3)+((IngredientSize%3!=0)?1:0),3,0,0));

    }

    private void initOldTables() {
        JTableHeader header = tblOldStockList.getTableHeader();
        header.setFont(CommonFont.getFontTitle());
        tblOldStockList.setFont(CommonFont.getFont());

        tblOldStockList.setModel(new AbstractTableModel() {
            String[] columnNames = { "ID", "Name", "Minneed", "Quantity" };

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return oldListIngredients.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ingredient ingredient = oldListIngredients.get(rowIndex);

                switch (columnIndex) {

                    case 0:
                        return ingredient.getId();
                    case 1:
                        return ingredient.getName();
                    case 2:
                        return ingredient.getMinNeed();
                    case 3:
                        return ingredient.getQuantity();
                    default:
                        return "unknow";

                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                Ingredient ingredient = oldListIngredients.get(rowIndex);
                if (columnIndex == 2) {
                    int minNeed = Integer.parseInt((String) aValue);
                    if (minNeed < 1) {
                        return;
                    }
                    ingredient.setMinNeed(minNeed);
                }
            }

        });

        tblOldStockList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblOldStockList.rowAtPoint(e.getPoint());
                int col = tblOldStockList.columnAtPoint(e.getPoint());
                Ingredient ingredient = oldListIngredients.get(row);
                Ingredient in = new Ingredient(ingredient.getId(), ingredient.getName(), ingredient.getMinNeed(),
                        ingredient.getQuantity());

            }

        });
    }

    private void initNewTables() {
        JTableHeader headers = tblNewStockList.getTableHeader();
        headers.setFont(CommonFont.getFontTitle());
        tblNewStockList.setFont(CommonFont.getFont());

        tblNewStockList.setModel(new AbstractTableModel() {
            String[] columnNames = { "ID", "Name", "Min Need", "Quantity" };

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return newListIngredients.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ingredient ingredient = newListIngredients.get(rowIndex);

                return switch (columnIndex) {
                    case 0 ->
                        ingredient.getId();
                    case 1 ->
                        ingredient.getName();
                    case 2 ->
                        ingredient.getMinNeed();
                    case 3 ->
                        ingredient.getQuantity();
                    default ->
                        "unknow";
                };

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                Ingredient oldIngredient = oldListIngredients.get(rowIndex);
                editIngredient = newListIngredients.get(rowIndex);
                // int minNeed = Integer.parseInt((String) aValue);
                // if(minNeed<1) return;
                // ingredient.setMinNeed(minNeed);
                try {
                    int oldQuantity = oldIngredient.getQuantity();
                    int quantity = Integer.parseInt((String) aValue);
                    if (quantity <= oldQuantity) {
                        editIngredient.setQuantity(quantity);
                        newListIngredients.set(rowIndex, editIngredient);
                        totalUnitLoss += oldQuantity - quantity;
                        fireTableCellUpdated(rowIndex, columnIndex);
                    } else {
                        JOptionPane.showMessageDialog(rootPane,
                                "You can't update value more than old ingredient in Check Stock Method !",
                                "Wrong Method !", JOptionPane.ERROR_MESSAGE);
                    }

                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                if (columnIndex == 3) {
                    return true;
                }
                return false;
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblNewStockList = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblOldStockList = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        taDes = new javax.swing.JTextArea();
        lbDesc = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 204, 153));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Stock List");

        tblNewStockList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblNewStockList.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][] {
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null }
                },
                new String[] {
                        "ID", "Name", "MinNeed", "Quantity"
                }));
        jScrollPane2.setViewportView(tblNewStockList);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("Update Item List");

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 102, 51));
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        tblOldStockList.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblOldStockList.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][] {
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null }
                },
                new String[] {
                        "Title 1", "Title 2", "Title 3", "Title 4"
                }));
        jScrollPane1.setViewportView(tblOldStockList);

        taDes.setColumns(20);
        taDes.setRows(5);
        jScrollPane3.setViewportView(taDes);

        lbDesc.setText("Description:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 120,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 280,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(jLabel2,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 187,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                                .addGap(6, 6, 6)
                                                                .addComponent(jScrollPane2,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 346,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout
                                                .createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGroup(jPanel1Layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(lbDesc)
                                                        .addGroup(jPanel1Layout
                                                                .createParallelGroup(
                                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                                                        jPanel1Layout.createSequentialGroup()
                                                                                .addComponent(btnCancel)
                                                                                .addPreferredGap(
                                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                                .addComponent(btnSave))
                                                                .addComponent(jScrollPane3,
                                                                        javax.swing.GroupLayout.Alignment.TRAILING,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 352,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addContainerGap()));
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 365,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8,
                                                        Short.MAX_VALUE)
                                                .addComponent(lbDesc)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 38,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel1Layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(btnSave)
                                                        .addComponent(btnCancel)))
                                        .addComponent(jScrollPane1))
                                .addContainerGap()));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap()));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }// GEN-LAST:event_btnCancelActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSaveActionPerformed
        if (editIngredient != null) {
            String description = taDes.getText();
            for (Ingredient newIngredient : newListIngredients) {
                ingredientService.update(newIngredient);
            }
            CheckStock cs = new CheckStock(user.getEmployee(), description, 0, totalUnitLoss, 0,
                    CommonDateTime.getDateNowString());
            checkStockServices.addNew(cs);
            JOptionPane.showMessageDialog(rootPane, "Update Ingredient Succeed !", "Succeed",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }// GEN-LAST:event_btnSaveActionPerformed

    /**
     * @param args the command line arguments
     */
    // public static void main(String args[]) {
    // /* Set the Nimbus look and feel */
    // //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code
    // (optional) ">
    // /* If Nimbus (introduced in Java SE 6) is not available, stay with the
    // default look and feel.
    // * For details see
    // http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
    // */
    // try {
    // for (javax.swing.UIManager.LookAndFeelInfo info :
    // javax.swing.UIManager.getInstalledLookAndFeels()) {
    // if ("Nimbus".equals(info.getName())) {
    // javax.swing.UIManager.setLookAndFeel(info.getClassName());
    // break;
    // }
    // }
    // } catch (ClassNotFoundException ex) {
    // java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE,
    // null, ex);
    // } catch (InstantiationException ex) {
    // java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE,
    // null, ex);
    // } catch (IllegalAccessException ex) {
    // java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE,
    // null, ex);
    // } catch (javax.swing.UnsupportedLookAndFeelException ex) {
    // java.util.logging.Logger.getLogger(EditDialog.class.getName()).log(java.util.logging.Level.SEVERE,
    // null, ex);
    // }
    // //</editor-fold>
    //
    // /* Create and display the dialog */
    // java.awt.EventQueue.invokeLater(new Runnable() {
    // public void run() {
    // EditDialog dialog = new EditDialog(new javax.swing.JFrame(), true);
    // dialog.addWindowListener(new java.awt.event.WindowAdapter() {
    // @Override
    // public void windowClosing(java.awt.event.WindowEvent e) {
    // System.exit(0);
    // }
    // });
    // dialog.setVisible(true);
    // }
    // });
    // }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbDesc;
    private javax.swing.JTextArea taDes;
    private javax.swing.JTable tblNewStockList;
    private javax.swing.JTable tblOldStockList;
    // End of variables declaration//GEN-END:variables
}
