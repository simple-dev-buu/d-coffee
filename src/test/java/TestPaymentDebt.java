
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.PaymentDebt;
import com.simpledev.d.coffee.services.CheckTimeServices;
import com.simpledev.d.coffee.services.EmployeeServices;
import com.simpledev.d.coffee.services.PaymentDebtServices;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Pee
 */
public class TestPaymentDebt {
    public static void main(String[] args) {
         PaymentDebtServices paymentDebtServices = new PaymentDebtServices();
         EmployeeServices employeeServices = new EmployeeServices();
         CheckTimeServices checkTimeServices = new CheckTimeServices();
         
         Employee emp1 = employeeServices.getById(3);
         CheckTime checkTime1 = checkTimeServices.getByEmpId(3);
         
         PaymentDebt payDebt1 = new PaymentDebt(emp1, checkTime1.getDate(), calculateSalary(emp1, checkTime1));
         
         paymentDebtServices.addNew(payDebt1);
         
         for(PaymentDebt pd : paymentDebtServices.getAll()) {
             System.out.println(pd);
         }
    }
    
    public static double calculateSalary(Employee emp, CheckTime checkTime) {
        double amount;
        double hourlyRate = emp.getMoneyRate();
        int totalHoursWorked = checkTime.getTotalWorked();

        amount = hourlyRate * totalHoursWorked;

        return amount;
    }
}
