package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BillLine {

    private int id;
    private int bill_id;
    private String name;
    private double amount;

    public BillLine() {
        this.id = -1;
        this.bill_id = -1;
        this.name = "";
        this.amount = 0;
    }

    public BillLine(int id,int bill_id, String name, double amount) {
        this.id = id;
        this.bill_id = bill_id;
        this.name = name;
        this.amount = amount;
    }

    public BillLine(int bill_id, String name, double amount) {
        this.bill_id = bill_id;
        this.name = name;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBill_id() {
        return bill_id;
    }

    public void setBill_id(int bill_id) {
        this.bill_id = bill_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "BillLine{" + "id=" + id + ", bill_id=" + bill_id + ", name=" + name + ", amount=" + amount + '}';
    }

    public static BillLine fromRS(ResultSet rs) {
        BillLine bill = new BillLine();
        try {
            bill.setId(rs.getInt("bl_id"));
            bill.setBill_id(rs.getInt("b_id"));
            bill.setName(rs.getString("bl_name"));
            bill.setAmount(rs.getDouble("bl_amount"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;

    }
}
