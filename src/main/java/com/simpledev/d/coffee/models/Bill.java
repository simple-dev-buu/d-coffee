package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.BillLineDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Bill {

    private int id;
    private double amount;
    private String date;
    private ArrayList<BillLine> lines;

    public Bill(double amount, String date, ArrayList<BillLine> lines) {
        this.id = -1;
        this.amount = amount;
        this.date = date;
        this.lines = lines;
    }

    public Bill(int id, double amount, String date, ArrayList<BillLine> lines) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.lines = lines;
    }

    public Bill() {
        this.id = -1;
        this.amount = 0;
        this.date = "";
        this.lines = null;
    }

    public ArrayList<BillLine> getLines() {
        return lines;
    }

    public void setLines(ArrayList<BillLine> lines) {
        this.lines = lines;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", amount=" + amount + ", date=" + date + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("b_id"));
            bill.setDate(rs.getString("b_date"));
            bill.setAmount(rs.getDouble("b_amount"));
            bill.setLines((ArrayList<BillLine>) new BillLineDao().getAllOf(bill));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;

    }
}
