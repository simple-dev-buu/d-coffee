package com.simpledev.d.coffee.ui.salary;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.CheckTime;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.PaymentDebt;
import com.simpledev.d.coffee.models.SalaryPayment;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.CheckTimeServices;
import com.simpledev.d.coffee.services.EmployeeServices;
import com.simpledev.d.coffee.services.PaymentDebtServices;
import com.sun.source.tree.BreakTree;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class DebtRecords extends javax.swing.JPanel {

    private final User user;
    private final PaymentDebtServices paymentDeptService;
    private EmployeeServices employeeServices;
    private CheckTimeServices checkTimeServices;
    private ArrayList<PaymentDebt> debts;
    private Predicate<PaymentDebt> filterFunction;
    private SearchSystem<PaymentDebt> searchSystem;
    private String searchValue;
    private int searchValueInt;
    private final EmployeeServices es;

    public DebtRecords(User user) {
        this.es = new EmployeeServices();
        this.paymentDeptService = new PaymentDebtServices();
        this.checkTimeServices = new CheckTimeServices();
        debts = paymentDeptService.getAll();
        initComponents();
        initTables();
        initSearchSystem();
        this.user = user;
    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                filterFunction = dept -> dept.getEmployee().getId() == searchValueInt;
            }
            case 1 -> {
                filterFunction = dept -> dept.getStartDate().contains(searchValue);
            }
            case 2 -> {
                filterFunction = dept -> (dept.getEmployee().getFirstName() + dept.getEmployee().getLastName()).toLowerCase().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, debts);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText().toLowerCase();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void reloadTables() {
        debts = searchSystem.getResults();
        tbEmpList.revalidate();
        tbEmpList.repaint();
    }

    private void refreshTables() {
        debts = paymentDeptService.getAll();
        tbEmpList.revalidate();
        tbEmpList.repaint();
    }

    private void initTables() {
        String[] filterOptions = {"ID", "DATE","EMP Name"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        tbEmpList.setFont(CommonFont.getFont());

        tbEmpList.setModel(new AbstractTableModel() {

            String[] headers = {"Employee ID","Employee Name", "Start Date", "Amount"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return debts.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                PaymentDebt dept = debts.get(rowIndex);

                return switch (columnIndex) {
                    case 0 ->
                        dept.getEmployee().getId();
                    case 1 ->
                        dept.getEmployee().getFirstName()+" "+dept.getEmployee().getLastName();
                    case 2 ->
                        dept.getStartDate();
                    case 3 ->
                        dept.getAmount();
                    default ->
                        "unknown";
                };
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCategory = new javax.swing.ButtonGroup();
        btnGroupStatusPayment = new javax.swing.ButtonGroup();
        btnCalculate = new javax.swing.JButton();
        pnlTop = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        btnPay = new javax.swing.JButton();
        searchFilter = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbEmpList = new javax.swing.JTable();

        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        pnlTop.setBackground(new java.awt.Color(153, 102, 0));

        jLabel3.setText("Employee Search");

        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        btnPay.setText("Pay");
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        searchFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFilterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlTopLayout = new javax.swing.GroupLayout(pnlTop);
        pnlTop.setLayout(pnlTopLayout);
        pnlTopLayout.setHorizontalGroup(
            pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTopLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlTopLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlTopLayout.createSequentialGroup()
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPay)))
                .addContainerGap())
        );
        pnlTopLayout.setVerticalGroup(
            pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTopLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPay))
                .addGap(18, 18, 18))
        );

        tbEmpList.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Period ", "Paid Date", "Employee ID", "Name", "Time Worked", "Number of days", "Net Salary", "State"
            }
        ));
        jScrollPane1.setViewportView(tbEmpList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        int selectedRow = 0;
        try {
            selectedRow = tbEmpList.getSelectedRow();
            PaymentDebt debt = debts.get(selectedRow);
            Employee emp = debt.getEmployee();
            Employee payer = user.getEmployee();
            PayDebtDialog pdd = new PayDebtDialog(payer, emp, debt);
            pdd.setLocationRelativeTo(this);
            pdd.setVisible(true);
            pdd.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    refreshTables();
                }
            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Please select row first !", "Error !", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnPayActionPerformed

    private void searchFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchFilterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchFilterActionPerformed

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchBarActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
//        CheckTime checkTime;
//        Employee employee;
//        for(PaymentDebt debt : debts) {
//            employee = debt.getEmployee();
//            checkTime = checkTimeServices.getByEmpId(employee.getId());
//            Double totalAmount = calculateSalary(employee, checkTime, debt);
//            debt.setAmount(totalAmount);
//            paymentDeptService.update(debt);
//        }
//        refreshTables();
    }//GEN-LAST:event_btnCalculateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.ButtonGroup btnCategory;
    private javax.swing.ButtonGroup btnGroupStatusPayment;
    private javax.swing.JButton btnPay;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlTop;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tbEmpList;
    // End of variables declaration//GEN-END:variables

    private double calculateSalary(Employee employee, CheckTime checkTime, PaymentDebt debt) {
        boolean isCalculated = false;
        double totalAmount = debt.getAmount();
        double hourlyRate = employee.getMoneyRate();
        int totalHoursWorked = checkTime.getTotalWorked();
        
        String dateStr1 = checkTime.getDate();
        String dateStr2 = debt.getStartDate();
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date1 = dateFormat.parse(dateStr1);
            Date date2 = dateFormat.parse(dateStr2);
            
            if( isCalculated == false) {
                if (date1.compareTo(date2) > 0 ) {
                    totalAmount = totalAmount + hourlyRate * totalHoursWorked;
                    isCalculated = true;
                } else if (date1.compareTo(date2) == 0) {
                    totalAmount = totalAmount + hourlyRate * totalHoursWorked;
                    isCalculated = true;
                } else {
                    return totalAmount;
                }
            } else {
                return totalAmount;
            }
//            if (date1.compareTo(date2) > 0 ) {
//                totalAmount = totalAmount + hourlyRate * totalHoursWorked;
//            } else if (date1.compareTo(date2) == 0) {
//                totalAmount = totalAmount + hourlyRate * totalHoursWorked;
//            } else {
//                return totalAmount;
//            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return totalAmount;
    }

}
