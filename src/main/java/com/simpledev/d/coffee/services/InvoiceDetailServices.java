
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.InvoiceDetailDao;
import com.simpledev.d.coffee.models.InvoiceDetail;
import com.simpledev.d.coffee.models.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InvoiceDetailServices {

    public ArrayList<InvoiceDetail> getAll() {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return (ArrayList<InvoiceDetail>) invoiceDetailDao.getAll();
    }

    public List<InvoiceDetail> getUsersByOrder(String where, String order) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return invoiceDetailDao.getAll(where, order);
    }

    public List<InvoiceDetail> getInvoiceDetailsByInvoiceId(int id) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return invoiceDetailDao.getAllByInvoiceId(id);
    }

    public List<InvoiceDetail> getInvoiceDetailByOrderDESC() {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        return invoiceDetailDao.getAll("invoiceDetail_id", "DESC");
    }

    public void addNew(InvoiceDetail editedStockDetail) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        invoiceDetailDao.insert(editedStockDetail);
    }

    public void update(InvoiceDetail editedStockDetail) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        invoiceDetailDao.update(editedStockDetail);
    }

    public void delete(InvoiceDetail editedStockDetail) {
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();
        invoiceDetailDao.delete(editedStockDetail);
    }

    public Map<Product, Integer> getGoodProductSold() {
        return new InvoiceDetailDao().getGoodProductSold();
    }

    public Map<Product, Integer> getBadProductSold() {
        return new InvoiceDetailDao().getBadProductSold();
    }
}
