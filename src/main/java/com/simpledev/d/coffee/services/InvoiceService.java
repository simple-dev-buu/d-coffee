
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.InvoiceDao;
import com.simpledev.d.coffee.dao.InvoiceDetailDao;
import com.simpledev.d.coffee.models.Invoice;
import com.simpledev.d.coffee.models.InvoiceDetail;
import java.util.ArrayList;
import java.util.List;

public class InvoiceService {

    public ArrayList<Invoice> getAll() {
        InvoiceDao inv = new InvoiceDao();
        return (ArrayList<Invoice>) inv.getAll();
    }

    public List<Invoice> getUsersByOrder(String where, String order) {
        InvoiceDao inv = new InvoiceDao();
        return inv.getAll(where, order);
    }

    public ArrayList<Invoice> getInvoiceByOrderDESC() {
        InvoiceDao inv = new InvoiceDao();
        return (ArrayList<Invoice>) inv.getAll("invoice_id", "DESC");
    }

    public void addNew(Invoice editedInvoice) {
        InvoiceDao inv = new InvoiceDao();
        InvoiceDetailDao invoiceDetailDao = new InvoiceDetailDao();

        Invoice invoice = inv.insert(editedInvoice);
        for (InvoiceDetail object : editedInvoice.getInvoiceDetails()) {
            object.setInvoice(invoice);
            object.getInvoice().setId(invoice.getId());
            invoiceDetailDao.insert(object);
        }
    }

    public void update(Invoice editedInvoice) {
        InvoiceDao inv = new InvoiceDao();
        inv.update(editedInvoice);
    }

    public void delete(Invoice editedInvoice) {
        InvoiceDao inv = new InvoiceDao();
        inv.delete(editedInvoice);
    }

    public double getAllNet() {
        ArrayList<Invoice> invoices = getAll();
        double net = 0;
        for (Invoice i : invoices) {
            net += i.getNetPrice();
        }
        return net;
    }

}
