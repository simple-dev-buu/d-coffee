package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Bill;
import com.simpledev.d.coffee.models.BillLine;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BillLineDao implements Dao<BillLine> {

    @Override
    public BillLine get(int id) {
        BillLine bill = new BillLine();
        String sql = "SELECT * FROM BILL_LINES WHERE bl_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = BillLine.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return bill;
    }

    @Override
    public List<BillLine> getAll() {
        BillLine bill;
        String sql = "SELECT * FROM BILL_LINES";
        ArrayList<BillLine> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                bill = BillLine.fromRS(rs);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillLine insert(BillLine obj) {
        String sql = "INSERT INTO BILL_LINES (b_id, bl_name, bl_amount) VALUES (?, ?, ?) ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBill_id());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getAmount());
            stmt.executeUpdate();
            int insertId = DatabaseHelper.getInsertedId(conn);
            obj.setId(insertId);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillLine update(BillLine obj) {
        String sql = "UPDATE BILL_LINES SET bl_name = ?, bl_amount = ? WHERE bl_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getAmount());
            stmt.setInt(3, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillLine obj) {
        String sql = "DELETE FROM BILL_LINES WHERE bl_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<BillLine> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<BillLine> getAllOf(Bill bill) {
        String sql = "SELECT * FROM BILL_LINES WHERE b_id = "+bill.getId();
        BillLine billLine;
        ArrayList<BillLine> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                billLine = BillLine.fromRS(rs);
                list.add(billLine);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
