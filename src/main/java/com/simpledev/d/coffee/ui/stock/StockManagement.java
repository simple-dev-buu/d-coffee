package com.simpledev.d.coffee.ui.stock;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.Ingredient;
import com.simpledev.d.coffee.models.User;
import com.simpledev.d.coffee.services.IngredientService;
import com.simpledev.d.coffee.ui.bill.BillRecords;
import com.simpledev.d.coffee.ui.receipt.ReceiptDialog;
import com.simpledev.d.coffee.ui.receipt.ReceiptRecords;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

public class StockManagement extends javax.swing.JPanel {

    private ArrayList<Ingredient> listIngredients;
    private Ingredient editedIngredient;

    private final IngredientService ingredientService;
    private String searchValue;
    private int searchValueInt;
    private SearchSystem<Ingredient> searchSystem;
    private Predicate<Ingredient> filterFunction;
    private Ingredient checkIngre;
    private User user;

    public StockManagement(User user) {
        this.user = user;
        ingredientService = new IngredientService();
        listIngredients = ingredientService.getIngredientOrderById();
        initComponents();
        initTables();
        initSearchSystem();

    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // name
                filterFunction = ind -> ind.getName().contains(searchValue);
            }
            case 1 -> {
                // ID
                filterFunction = ind -> ind.getId() == searchValueInt;
            }

            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchBar.setFont(CommonFont.getFont());
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, listIngredients);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = {"name", "id"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tblStocklist.setRowHeight(100);
        tblStocklist.setFont(CommonFont.getFont());
        panelHead.setFont(CommonFont.getFont());
        JTableHeader header = tblStocklist.getTableHeader();
        header.setFont(CommonFont.getFont());

        listIngredients = ingredientService.getAll();
        tblStocklist.setModel(new AbstractTableModel() {
            String[] columnNames = {"Image", "ID", "Name", "Minneed", "Quantity"};

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return listIngredients.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Ingredient ingredient = listIngredients.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("assets/ingredient/ingredient" + ingredient.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100.0 * width) / height, 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return ingredient.getId();
                    case 2:
                        return ingredient.getName();
                    case 3:
                        return ingredient.getMinNeed();
                    case 4:
                        return ingredient.getQuantity();

                    default:
                        return "unknow";

                }

            }

        });
        tblStocklist.setRowHeight(100);
        tblStocklist.setFont(CommonFont.getFont());
        panelHead.setFont(CommonFont.getFont());
        JTableHeader headers = tblStocklist.getTableHeader();
        headers.setFont(CommonFont.getFont());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        contentPand = new javax.swing.JScrollPane();
        tblStocklist = new javax.swing.JTable();
        panelHead = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        btnMakeReceipt = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        searchFilter = new javax.swing.JComboBox<>();
        btnPrint = new javax.swing.JButton();
        btnCheckStock = new javax.swing.JButton();

        contentPand.setBackground(new java.awt.Color(255, 204, 204));

        tblStocklist.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblStocklist.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Ingredient ID", "Ingredient Name", "Minneed", "Quantity", "Value"
            }
        ));
        contentPand.setViewportView(tblStocklist);

        panelHead.setBackground(new java.awt.Color(255, 204, 102));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Ingredient Search");

        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        btnMakeReceipt.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnMakeReceipt.setText("Make Receipt");
        btnMakeReceipt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMakeReceiptActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnCheckStock.setText("Check Stock");
        btnCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckStockActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelHeadLayout = new javax.swing.GroupLayout(panelHead);
        panelHead.setLayout(panelHeadLayout);
        panelHeadLayout.setHorizontalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeadLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelHeadLayout.createSequentialGroup()
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                        .addComponent(btnCheckStock)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnMakeReceipt)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete))
                    .addComponent(jLabel2))
                .addContainerGap())
        );
        panelHeadLayout.setVerticalGroup(
            panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeadLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMakeReceipt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint)
                    .addComponent(btnCheckStock))
                .addGap(21, 21, 21))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelHead, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contentPand))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelHead, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(contentPand, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchBarActionPerformed

    private void btnMakeReceiptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMakeReceiptActionPerformed
        editedIngredient = new Ingredient();
        openDialog();
    }//GEN-LAST:event_btnMakeReceiptActionPerformed

    private void btnPrintIngredientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintIngredientActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnPrintIngredientActionPerformed

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ReceiptDialog rd = new ReceiptDialog(frame);
        rd.setTitle("Receipt Records");
        rd.setLocationRelativeTo(this);
        rd.setVisible(true);
        rd.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }
        });
    }

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblStocklist.getSelectedRow();
        if (selectedIndex >= 0) {
            editedIngredient = listIngredients.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                ingredientService.delete(editedIngredient);
            }
            refreshTables();
            ingredientService.delete(editedIngredient);
        }
        refreshTables();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        openInventoryCheckDialog();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckStockActionPerformed
        openUpdateStockDialog();
    }//GEN-LAST:event_btnCheckStockActionPerformed

//    private void openDialog() {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
//        StockDialog stockDialog = new StockDialog(frame, editedIngredient);
//        stockDialog.setTitle("Stock Management");
//        stockDialog.setLocationRelativeTo(this);
//        stockDialog.setVisible(true);
//        stockDialog.addWindowListener(new WindowAdapter() {
//            @Override
//            public void windowClosed(WindowEvent e) {
//                refreshTables();
//            }
//
//        });
//    }
    private void openUpdateStockDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        UpdateStockDialog editDialog = new UpdateStockDialog(frame, listIngredients, user);
        editDialog.setTitle("Check Stock");
        editDialog.setLocationRelativeTo(this);
        editDialog.setVisible(true);
        editDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }

        });
    }

    private void openInventoryCheckDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InventoryCheckStock invCheckStock;
        invCheckStock = new InventoryCheckStock(frame, checkIngre);
        invCheckStock.setTitle("Check Stock");
        invCheckStock.setLocationRelativeTo(this);
        invCheckStock.setVisible(true);
        invCheckStock.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }

        });

    }

    private void reloadTables() {
        listIngredients = searchSystem.getResults();
        tblStocklist.revalidate();
        tblStocklist.repaint();
    }

    private void refreshTables() {
        listIngredients = ingredientService.getAll();
        tblStocklist.revalidate();
        tblStocklist.repaint();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckStock;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnMakeReceipt;
    private javax.swing.JButton btnPrint;
    private javax.swing.JScrollPane contentPand;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel panelHead;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tblStocklist;
    // End of variables declaration//GEN-END:variables

}
