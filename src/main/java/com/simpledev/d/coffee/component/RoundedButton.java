package com.simpledev.d.coffee.component;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

public class RoundedButton extends JButton {

    private final Color backgroundColor;
    private final Color textColor;

    public RoundedButton(String text, Color backgroundColor, Color textColor) {
        super(text);
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
        
        setHorizontalTextPosition(SwingConstants.CENTER);
        setContentAreaFilled(false); // Set to false to make the background transparent
        setFocusPainted(false); // Remove the focus border
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();

        // Set rendering hints for smooth rendering
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int width = getWidth();
        int height = getHeight();

        RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(0, 0, width, height, 30, 30); // Adjust the radius as needed
        g2.setColor(backgroundColor);
        g2.fill(roundedRectangle);

        g2.setColor(textColor);
        g2.setFont(getFont());
        FontMetrics fontMetrics = g2.getFontMetrics();
        int textX = (width - fontMetrics.stringWidth(getText())) / 2;
        int textY = (height - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent();
        g2.drawString(getText(), textX, textY);

        g2.dispose();
    }
}
