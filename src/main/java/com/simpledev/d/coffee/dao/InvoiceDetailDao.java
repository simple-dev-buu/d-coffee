package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.InvoiceDetail;
import com.simpledev.d.coffee.models.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;


public class InvoiceDetailDao implements Dao<InvoiceDetail> {

    @Override
    public InvoiceDetail get(int id) {
        InvoiceDetail invoiceDetail = new InvoiceDetail();
        String sql = "SELECT * FROM INVOICE_DETAIL WHERE invoiceDetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                invoiceDetail = InvoiceDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return invoiceDetail;
    }

    @Override
    public List<InvoiceDetail> getAll() {
        ArrayList<InvoiceDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM INVOICE_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                InvoiceDetail invoiceDetail = new InvoiceDetail();
                list.add(invoiceDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public InvoiceDetail insert(InvoiceDetail obj) {
        String sql = "INSERT INTO INVOICE_DETAIL (product_id, invoice_id, invoiceDetail_unit, invoiceDetail_unitPrice, invoiceDetail_discount, invoiceDetail_netPrice)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getInvoice().getId());
            stmt.setInt(3, obj.getUnit());
            stmt.setDouble(4, obj.getUnitprice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(conn);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public InvoiceDetail update(InvoiceDetail obj) {
        String sql = "UPDATE INVOICE_DETAIL"
                + " SET product_id = ?, invoice_id = ?, invoiceDetail_unit = ?, invoiceDetail_unitPrice = ?, invoiceDetail_discount = ?, invoiceDetail_netPrice = ?"
                + " WHERE invoiceDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getInvoice().getId());
            stmt.setInt(3, obj.getUnit());
            stmt.setDouble(4, obj.getUnitprice());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(InvoiceDetail obj) {
        String sql = "DELETE FROM INVOICE_DETAIL WHERE invoiceDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<InvoiceDetail> getAll(String where, String order) {
        InvoiceDetail invoiceDetail;
        ArrayList<InvoiceDetail> list = new ArrayList();
        String sql = "SELECT * FROM INVOICE_DETAIL  ORDER BY " + where + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                invoiceDetail = InvoiceDetail.fromRS(rs);
                list.add(invoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<InvoiceDetail> getAllByInvoiceId(int id) {
        InvoiceDetail invoiceDetail;
        ArrayList<InvoiceDetail> list = new ArrayList();
        String sql = "SELECT * FROM INVOICE_DETAIL  WHERE invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                invoiceDetail = InvoiceDetail.fromRS(rs);
                list.add(invoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Map<Product, Integer> getGoodProductSold() {
        ProductDao pd = new ProductDao();
        Map<Product, Integer> map = new HashMap<>();
        String sql = "SELECT DISTINCT product_id, SUM(invoiceDetail_unit) AS total_amount "
                + "FROM invoice_detail "
                + "GROUP BY product_id "
                + "HAVING total_amount >= 10 ORDER BY total_amount DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int p_id = rs.getInt("product_id");
                map.put(pd.get(p_id), rs.getInt("total_amount"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return map;
    }

    public Map<Product, Integer> getBadProductSold() {
        ProductDao pd = new ProductDao();
        Map<Product, Integer> map = new HashMap<>();
        String sql = "SELECT DISTINCT product_id, SUM(invoiceDetail_unit) AS total_amount "
                + "FROM invoice_detail "
                + "GROUP BY product_id "
                + "HAVING total_amount < 10 ORDER BY total_amount ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int p_id = rs.getInt("product_id");
                map.put(pd.get(p_id), rs.getInt("total_amount"));

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return map;
    }

}
