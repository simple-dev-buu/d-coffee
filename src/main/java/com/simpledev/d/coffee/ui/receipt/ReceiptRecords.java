package com.simpledev.d.coffee.ui.receipt;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.Receipt;
import com.simpledev.d.coffee.services.ReceiptService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

public class ReceiptRecords extends javax.swing.JPanel {

    private final ReceiptService receiptService;
    private ArrayList<Receipt> receiptList;
    private Receipt editedReceipt;
    private Predicate<Receipt> filterFunction;
    private SearchSystem<Receipt> searchSystem;
    private String searchValue;
    private int searchValueInt;

    public ReceiptRecords() {
        initComponents();
        receiptService = new ReceiptService();
        receiptList = receiptService.getAll();
        initTables();
        initSearchSystem();
        tblRCRecords.setFont(CommonFont.getFont());
        panelHead.setFont(CommonFont.getFont());
        JTableHeader header = tblRCRecords.getTableHeader();
        header.setFont(CommonFont.getFont());

    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // emp name
                filterFunction = obj -> (obj.getEmployee().getFirstName() + obj.getEmployee().getLastName())
                        .toLowerCase().contains(searchValue);
            }
            case 1 -> {
                // date order
                filterFunction = obj -> obj.getDateOrder().contains(searchValue);
            }

            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, receiptList);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = { "Emp Name", "Date" };
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tblRCRecords.setModel(new AbstractTableModel() {
            String[] headers = { "ID", "EmpName", "Vendor Code", "Qty", "Total", "Discount", "Net Price", "Date Order",
                    "Date Receive", "Date Paid", "Change", "Status" };

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return receiptList.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Receipt rc = receiptList.get(rowIndex);
                return switch (columnIndex) {
                    case 0 ->
                        rc.getId();
                    case 1 ->
                        rc.getEmployee().getFirstName() + " " + rc.getEmployee().getLastName();
                    case 2 ->
                        rc.getVendor().getCode();
                    case 3 ->
                        rc.getQuantity();
                    case 4 ->
                        rc.getTotal();
                    case 5 ->
                        rc.getDiscount();
                    case 6 ->
                        rc.getNetPrice();
                    case 7 ->
                        rc.getDateOrder();
                    case 8 ->
                        rc.getDateReceive();
                    case 9 ->
                        rc.getDatePaid();
                    case 10 ->
                        rc.getChange();
                    case 11 ->
                        rc.getStatus();

                    default ->
                        "unknow";
                };
            }
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHead = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        searchFilter = new javax.swing.JComboBox<>();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRCRecords = new javax.swing.JTable();

        panelHead.setBackground(new java.awt.Color(255, 204, 102));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Search");

        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        searchFilter.setModel(
                new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelHeadLayout = new javax.swing.GroupLayout(panelHead);
        panelHead.setLayout(panelHeadLayout);
        panelHeadLayout.setHorizontalGroup(
                panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelHeadLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panelHeadLayout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addGroup(panelHeadLayout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel2)
                                                        .addGroup(panelHeadLayout.createSequentialGroup()
                                                                .addComponent(searchBar,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 419,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(searchFilter,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                                        349, Short.MAX_VALUE)
                                                                .addComponent(btnDelete,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE, 72,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addContainerGap())))
                                        .addGroup(panelHeadLayout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addContainerGap()))));
        panelHeadLayout.setVerticalGroup(
                panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelHeadLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelHeadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnDelete)
                                        .addGroup(panelHeadLayout
                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(34, Short.MAX_VALUE)));

        tblRCRecords.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][] {
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null },
                        { null, null, null, null }
                },
                new String[] {
                        "Title 1", "Title 2", "Title 3", "Title 4"
                }));
        jScrollPane1.setViewportView(tblRCRecords);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jScrollPane1)
                                        .addComponent(panelHead, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(panelHead, javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
                                .addContainerGap()));
    }// </editor-fold>//GEN-END:initComponents

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_searchBarActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblRCRecords.getSelectedRow();
        if (selectedIndex >= 0) {
            editedReceipt = receiptList.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                receiptService.delete(editedReceipt);
            }
            refreshTables();
            receiptService.delete(editedReceipt);
        }
        refreshTables();
    }// GEN-LAST:event_btnDeleteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelHead;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tblRCRecords;
    // End of variables declaration//GEN-END:variables

    private void reloadTables() {
        receiptList = searchSystem.getResults();
        tblRCRecords.revalidate();
        tblRCRecords.repaint();
    }

    private void refreshTables() {
        receiptList = receiptService.getAll();
        tblRCRecords.revalidate();
        tblRCRecords.repaint();
    }

}