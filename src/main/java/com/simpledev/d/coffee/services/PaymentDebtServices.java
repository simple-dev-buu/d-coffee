/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.PaymentDebtDao;
import com.simpledev.d.coffee.models.PaymentDebt;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class PaymentDebtServices {
    public ArrayList<PaymentDebt> getAll() {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return (ArrayList<PaymentDebt>) paymentDebtDao.getAll();
    }
    
    public PaymentDebt getByEmpId(int id) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.get(id);
    }
    
    public List<PaymentDebt> getPaymentDebtsByOrder(String where, String order) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.getAll(where, order);
    }
    
    public List<PaymentDebt> getPaymentDebtsOrderByASC() {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.getAll("paymentDebt_startDate", "ASC");
    }

    public PaymentDebt addNew(PaymentDebt editedPaymentDebt) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.insert(editedPaymentDebt);
    }

    public PaymentDebt update(PaymentDebt editedPaymentDebt) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.update(editedPaymentDebt);
    }

    public int delete(PaymentDebt editedPaymentDebt) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.delete(editedPaymentDebt);
    }
    
    public ArrayList<PaymentDebt> getByMonth(int month) {
        PaymentDebtDao paymentDebtDao = new PaymentDebtDao();
        return paymentDebtDao.getByMonth(month);
    }
}
