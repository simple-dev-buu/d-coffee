
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.models.Customer;
import java.util.ArrayList;
import java.util.List;


public class CustomerServices {
    public Customer getByTel(String tel) {
        CustomerDao customerDao = new CustomerDao();
        Customer customer = customerDao.getByTel(tel);
        return customer;
    }
    
    public Customer getById(int id) {
        CustomerDao customerDao = new CustomerDao();
        Customer customer = customerDao.get(id);
        return customer;
    }
    public Customer getByFName(String fname) {
        CustomerDao customerDao = new CustomerDao();
        Customer customer = customerDao.getByFName(fname);
        return customer;
    }
    
    public List<Customer> getCustomers() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(" customer_id ASC ");
    }
  
    public List<Customer> getCustomersDESC() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(" customer_id DESC ");
    }

    public ArrayList<Customer> getAll() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll();
    }
    public List<Customer> getUsersByOrder(String where, String order) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(where, order);
    }
    public Customer addNew(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.insert(editedCustomer);
    }
    public Customer update(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCustomer);
    }
    public int delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCustomer);
    }

}
