package com.simpledev.d.coffee.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonDateTime {
    
    private static final String DATE = "yyyy-MM-dd";
    private static final String TIME = "HH:mm:ss";
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DATE);
    private static final DateTimeFormatter STANDARD_FORMATTER = DateTimeFormatter.ofPattern(DATE + " " + TIME);
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE);
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME);
    
    public static SimpleDateFormat getSimpleDateFormat(){
        return FORMATTER;
    }
    
    public static DateTimeFormatter getFormatter() {
        return STANDARD_FORMATTER;
    }

    public static DateTimeFormatter getDateFormatter() {
        return DATE_FORMATTER;
    }

    public static DateTimeFormatter getTimeFormatter() {
        return TIME_FORMATTER;
    }
    
    public static LocalDateTime stringToDateTime(String dateString) {
        try {
            return LocalDateTime.parse(dateString, STANDARD_FORMATTER);
        } catch (DateTimeParseException e) {
            // Handle the exception or log an error.
            System.out.println(e);
            return null; // Return null as a sign of failure.
        }
    }

    public static String dateTimeToString(LocalDateTime dateTime) {
        return dateTime.format(STANDARD_FORMATTER);
    }

    public static String getDateTimeNowString() {
        return LocalDateTime.now().format(STANDARD_FORMATTER);
    }

    public static String getDateNowString() {
        return LocalDateTime.now().format(DATE_FORMATTER);
    }

    public static String getTimeNowString() {
        return LocalDateTime.now().format(TIME_FORMATTER);
    }
    
    public static Date stringToDate(String str){
        try {
            return FORMATTER.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(CommonDateTime.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
