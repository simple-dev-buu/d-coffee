
package com.simpledev.d.coffee.ui.salary;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.SalaryPayment;
import com.simpledev.d.coffee.services.SalaryPaymentService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class SalaryRecords extends javax.swing.JPanel {

    private SalaryPaymentService salaryPaymentService;
    private ArrayList<SalaryPayment> slpList;

    private Predicate<SalaryPayment> filterFunction;
    private SearchSystem<SalaryPayment> searchSystem;
    private String searchValue;
    private int searchValueInt;
    private SalaryPayment editedSalaryPayment;

    public SalaryRecords() {
        initComponents();
        salaryPaymentService = new SalaryPaymentService();
        slpList = salaryPaymentService.getAll();
        initTables();
        initSearchSystem();
    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // SLP ID
                filterFunction = slp -> slp.getId() == searchValueInt;
            }
            case 1 -> {
                // EMP name
                filterFunction = slp -> (slp.getEmp().getFirstName() + slp.getEmp().getLastName()).toLowerCase().contains(searchValue);
            }
            case 2 -> {
                // DATE
                filterFunction = slp -> slp.getDate().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, slpList);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText().toLowerCase();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void reloadTables() {
        slpList = searchSystem.getResults();
        tblSalary.revalidate();
        tblSalary.repaint();
    }

    private void refreshTables() {
        slpList = salaryPaymentService.getAll();
        tblSalary.revalidate();
        tblSalary.repaint();
    }

    private void refreshTablesForMonth() {
        tblSalary.revalidate();
        tblSalary.repaint();
    }

    private void initTables() {
        String[] filterOptions0 = { "ID", "Emp Name", "DATE" };
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions0));
        String[] filterOptions1 = { "All", "January", "February", "March", "April", "May", "June", "July", "August",
                "September", "October", "November", "December" };
        cmbMonth.setModel(new DefaultComboBoxModel<>(filterOptions1));
        cmbMonth.setSelectedIndex(0);
        tblSalary.setFont(CommonFont.getFont());

        tblSalary.setModel(new AbstractTableModel() {

            String[] headers = { "SalaryPaymentID",
                    "EmployeeName",
                    "Paid Date",
                    "Total Amount"
            };

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return slpList.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                SalaryPayment slp = slpList.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return slp.getId();
                    case 1:
                        return slp.getEmp().getFirstName()+" "+slp.getEmp().getLastName();
                    case 2:
                        return slp.getDate();
                    case 3:
                        return slp.getTotalAmount();

                    default:
                        return "unknown";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCategory = new javax.swing.ButtonGroup();
        btnGroupStatusPayment = new javax.swing.ButtonGroup();
        pnlTop = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        cmbMonth = new javax.swing.JComboBox<>();
        searchFilter = new javax.swing.JComboBox<>();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSalary = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 204, 204));

        pnlTop.setBackground(new java.awt.Color(255, 153, 153));

        jLabel3.setText("Employee Search");

        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        cmbMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        cmbMonth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbMonthActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        searchFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFilterActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlTopLayout = new javax.swing.GroupLayout(pnlTop);
        pnlTop.setLayout(pnlTopLayout);
        pnlTopLayout.setHorizontalGroup(
            pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTopLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlTopLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlTopLayout.createSequentialGroup()
                        .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbMonth, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlTopLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTopLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnDelete)
                                .addGap(19, 19, 19))))))
        );
        pnlTopLayout.setVerticalGroup(
            pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTopLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete))
                .addGap(0, 14, Short.MAX_VALUE))
        );

        tblSalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Period ", "Paid Date", "Employee ID", "Name", "Time Worked", "Number of days", "Net Salary", "State"
            }
        ));
        jScrollPane1.setViewportView(tblSalary);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblSalary.getSelectedRow();
        if (selectedIndex >= 0) {
            editedSalaryPayment = slpList.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                salaryPaymentService.delete(editedSalaryPayment);
            }
            refreshTables();
           
        }
        refreshTables();
    }//GEN-LAST:event_btnDeleteActionPerformed

//    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCreateActionPerformed
//        editedSalaryPayment = new SalaryPayment();
//        openDialog();
//    }// GEN-LAST:event_btnCreateActionPerformed

    private void cmbMonthActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmbMonthActionPerformed
        switch (cmbMonth.getSelectedIndex()) {
            case 0 -> slpList = salaryPaymentService.getAll();
            case 1 -> slpList = salaryPaymentService.getByMonth(1);
            case 2 -> slpList = salaryPaymentService.getByMonth(2);
            case 3 -> slpList = salaryPaymentService.getByMonth(3);
            case 4 -> slpList = salaryPaymentService.getByMonth(4);
            case 5 -> slpList = salaryPaymentService.getByMonth(5);
            case 6 -> slpList = salaryPaymentService.getByMonth(6);
            case 7 -> slpList = salaryPaymentService.getByMonth(7);
            case 8 -> slpList = salaryPaymentService.getByMonth(8);
            case 9 -> slpList = salaryPaymentService.getByMonth(9);
            case 10 -> slpList = salaryPaymentService.getByMonth(10);
            case 11 -> slpList = salaryPaymentService.getByMonth(11);
            case 12 -> slpList = salaryPaymentService.getByMonth(12);
        }
        refreshTablesForMonth();
    }// GEN-LAST:event_cmbMonthActionPerformed

    private void searchFilterActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_searchFilterActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_searchFilterActionPerformed

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }// GEN-LAST:event_searchBarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnCategory;
    private javax.swing.JButton btnDelete;
    private javax.swing.ButtonGroup btnGroupStatusPayment;
    private javax.swing.JComboBox<String> cmbMonth;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlTop;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tblSalary;
    // End of variables declaration//GEN-END:variables

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SalaryDialog salaryDialog = new SalaryDialog(frame, editedSalaryPayment);
        salaryDialog.setTitle("Salary Management");
        salaryDialog.setLocationRelativeTo(this);
        salaryDialog.setVisible(true);
        salaryDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTables();
            }

        });
    }
}
