package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Bill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BillDao implements Dao<Bill> {

    @Override
    public Bill get(int id) {
        Bill bill = new Bill();
        String sql = "SELECT * FROM BILL WHERE b_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = Bill.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return bill;

    }

    @Override
    public List<Bill> getAll() {
        Bill bill;
        String sql = "SELECT * FROM BILL";
        ArrayList<Bill> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                bill = Bill.fromRS(rs);
                list.add(bill);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Bill insert(Bill obj) {
        String sql = "INSERT INTO BILL (b_date, b_amount) VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setDouble(2, obj.getAmount());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                return null;  // No rows were inserted
            }
            
           int insertedId = DatabaseHelper.getInsertedId(conn);
           obj.setId(insertedId);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE BILL SET b_date = ?, b_amount = ? WHERE b_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDate());
            stmt.setDouble(2, obj.getAmount());
            stmt.setInt(3, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM BILL WHERE b_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
