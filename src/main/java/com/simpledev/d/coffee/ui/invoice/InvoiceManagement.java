package com.simpledev.d.coffee.ui.invoice;

import com.simpledev.d.coffee.common.CommonDateTime;
import com.simpledev.d.coffee.common.SearchSystem;
import com.simpledev.d.coffee.models.Invoice;
import com.simpledev.d.coffee.models.Promotion;
import com.simpledev.d.coffee.services.InvoiceService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.function.Predicate;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;

public class InvoiceManagement extends javax.swing.JPanel {

    private ArrayList<Invoice> listInvoice;
    private final InvoiceService rs;
    private String searchValue;
    private int searchValueInt;
    private SearchSystem<Invoice> searchSystem;
    private Predicate<Invoice> filterFunction;
    private Invoice invoice;

    public InvoiceManagement() {
        initComponents();
        rs = new InvoiceService();
        listInvoice = rs.getAll();
        initTables();
        initSearchSystem();

    }

    private void setFilter() {
        switch (searchFilter.getSelectedIndex()) {
            case 0 -> {
                // invoice id
                filterFunction = inv -> inv.getId() == searchValueInt;
            }
            case 1 -> {
                // emp id
                filterFunction = inv -> (inv.getEmployee().getFirstName() + inv.getEmployee().getLastName()).toLowerCase().contains(searchValue);
            }
            case 2 -> {
                // product name
                filterFunction = inv -> (inv.getCustomer().getFirstName() + inv.getCustomer().getLastName()).toLowerCase().contains(searchValue);
            }
            default ->
                throw new AssertionError();
        }
    }

    private int parseInputAsInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            // Handle invalid input here, e.g., display an error message
            return 0; // Default value or any suitable value for invalid input
        }
    }

    private void initSearchSystem() {
        searchValue = ""; // Initialize searchValue
        searchValueInt = 0; // Initialize searchValueInt
        setFilter(); // Initialize the filter function
        searchSystem = new SearchSystem<>(searchBar, listInvoice);

        searchBar.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSearchAndReloadTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Handle changedUpdate if needed
            }
        });
    }

    private void updateSearchAndReloadTables() {
        searchValue = searchBar.getText();
        searchValueInt = parseInputAsInt(searchValue);
        setFilter(); // Update the filter function
        searchSystem.update(searchValue, filterFunction);
        reloadTables();
    }

    private void initTables() {
        String[] filterOptions = {"Invoice ID", "Emp name", "Customer name"};
        searchFilter.setModel(new DefaultComboBoxModel<>(filterOptions));
        searchFilter.setSelectedIndex(0);

        tblListOrder.setModel(new AbstractTableModel() {
            String[] headers = {
                "Invoice ID",
                "Customer Name",
                "Employee Name",
                "Branch",
                "Promotion",
                "Date",
                "Time",
                "Total Price",
                "Discount",
                "Net Price",
                "Money Receive",
                "Money Change",};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return listInvoice.size();
            }

            @Override
            public int getColumnCount() {
                return headers.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                
                Invoice inv = listInvoice.get(rowIndex);
                Promotion promotion = inv.getPromotion();
                String dateTime = CommonDateTime.dateTimeToString(inv.getDate());
                String[] time = dateTime.split(" ");
                return switch (columnIndex) {
                    case 0 -> {
                        yield inv.getId();
                    }
                    case 1 -> {
                        yield inv.getCustomer().getFirstName()+" "+inv.getCustomer().getLastName();
                    }
                    case 2 -> {
                        yield inv.getEmployee().getFirstName()+" "+inv.getEmployee().getLastName();
                    }
                    case 3 -> {
                        yield inv.getBranch().getCode();
                    }
                    case 4 -> {
                        if (promotion != null) {
                           yield inv.getPromotion().getName();
                           
                        } else {
                            yield "-";
                        }
                    }
                    case 5 -> {
                        yield time[0];
                    }
                    case 6 -> {
                        yield time[1];
                    }
                    case 7 -> {
                        yield inv.getTotalPrice();
                    }
                    case 8 -> {
                        yield inv.getDiscount();
                    }
                    case 9 -> {
                        yield inv.getNetPrice();
                    }
                    case 10 -> {
                        yield inv.getMoneyReceive();
                    }
                    case 11 -> {
                        yield inv.getMoneyChange();
                    }
                    default -> {
                        yield "unknow";
                    }
                };

            }

        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        searchBar = new javax.swing.JTextField();
        btnPrint = new javax.swing.JButton();
        btnDeleteOrder = new javax.swing.JButton();
        searchFilter = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblListOrder = new javax.swing.JTable();

        jPanel1.setBackground(new java.awt.Color(255, 204, 153));

        jLabel1.setText("Order Search");

        searchBar.setForeground(new java.awt.Color(0, 0, 0));
        searchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBarActionPerformed(evt);
            }
        });

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnDeleteOrder.setText("Delete");
        btnDeleteOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteOrderActionPerformed(evt);
            }
        });

        searchFilter.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 291, Short.MAX_VALUE)
                        .addComponent(btnPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeleteOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPrint)
                    .addComponent(btnDeleteOrder)
                    .addComponent(searchFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblListOrder.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.", "Order ID", "Product Name", "Customer Type", "Price", "Employee ID", "Discount", "Getmoney"
            }
        ));
        jScrollPane1.setViewportView(tblListOrder);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchBarActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        
        int selectedIndex = tblListOrder.getSelectedRow();
        if (selectedIndex >= 0) {
            invoice = listInvoice.get(selectedIndex);
            openInvoiceManagementDialog();

        }
//        refreshTables();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnDeleteOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteOrderActionPerformed
        int selectedIndex = tblListOrder.getSelectedRow();
        if (selectedIndex >= 0) {
            invoice = listInvoice.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
               rs.delete(invoice);
            }
            refreshTables();
            rs.delete(invoice);
        }
    }//GEN-LAST:event_btnDeleteOrderActionPerformed

    private void openInvoiceManagementDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InvoiceManagementDialog invoiceManagementDialog = new InvoiceManagementDialog(frame, invoice);
        invoiceManagementDialog.setTitle("Print Receipt");
        invoiceManagementDialog.setLocationRelativeTo(this);
        invoiceManagementDialog.setVisible(true);
        invoiceManagementDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                System.out.println("Hi 5");
            }

        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteOrder;
    private javax.swing.JButton btnPrint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField searchBar;
    private javax.swing.JComboBox<String> searchFilter;
    private javax.swing.JTable tblListOrder;
    // End of variables declaration//GEN-END:variables

    private void reloadTables() {
        listInvoice = searchSystem.getResults();
        tblListOrder.revalidate();
        tblListOrder.repaint();
    }

    private void refresh() {
        listInvoice = rs.getAll();
        tblListOrder.revalidate();
        tblListOrder.repaint();
    }

    private void refreshTables() {
        listInvoice = rs.getInvoiceByOrderDESC();
        tblListOrder.revalidate();
        tblListOrder.repaint();
    }
}
