
package com.simpledev.d.coffee.component;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JDialog;


public class RoundedDialog extends JDialog {

    public RoundedDialog(Frame owner, String title, boolean modal) {
        super(owner, title, modal);
        setUndecorated(true);
        setLocationRelativeTo(owner);
    }

    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Graphics2D g2d = (Graphics2D) g;

        int width = getWidth();
        int height = getHeight();
        int arc = 20; // Adjust the arc size as needed for rounded corners

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(getBackground());

        RoundRectangle2D roundedRectangle = new RoundRectangle2D.Float(0, 0, width, height, arc, arc);
        g2d.fill(roundedRectangle);
    }
}