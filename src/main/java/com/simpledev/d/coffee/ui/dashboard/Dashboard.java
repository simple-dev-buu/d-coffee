package com.simpledev.d.coffee.ui.dashboard;

import com.simpledev.d.coffee.common.CommonFont;
import com.simpledev.d.coffee.models.Product;
import com.simpledev.d.coffee.services.BillService;
import com.simpledev.d.coffee.services.InvoiceDetailServices;
import com.simpledev.d.coffee.services.InvoiceService;
import com.simpledev.d.coffee.services.ReceiptService;
import com.simpledev.d.coffee.services.SalaryPaymentService;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.util.Map;
import javax.swing.table.AbstractTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

public class Dashboard extends javax.swing.JPanel {

    private final ReceiptService receiptService;
    private final InvoiceService invoiceService;
    private final InvoiceDetailServices invoiceDetailServices;
    private final BillService billService;
    private final SalaryPaymentService salaryPaymentService;
    private double income, expense, net;
    private final Map<Product, Integer> listGoodSold;
    private final Map<Product, Integer> listBadSold;

    public Dashboard() {
        invoiceDetailServices = new InvoiceDetailServices();
        listGoodSold = invoiceDetailServices.getGoodProductSold();
        listBadSold = invoiceDetailServices.getBadProductSold();
        invoiceService = new InvoiceService();
        receiptService = new ReceiptService();
        billService = new BillService();
        salaryPaymentService = new SalaryPaymentService();
        setPreferredSize(new Dimension(1080, 580));
        initComponents();
        initChart();
        initTables();
        initText();
        income = 0;
        expense = 0;
        net = 0;
    }

    private PieDataset createDataset() {
        income = invoiceService.getAllNet();
        expense = billService.getAllTotalExpense() + salaryPaymentService.getAllTotalPaid() + receiptService.getAllTotalPaid();
        net = income - expense;
        final DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Income", income);
        dataset.setValue("Expense", expense);

        return dataset;
    }

    private void initChart() {
        int width = 725;
        int height = 389;
        pnlChartTop.setLayout(new GridBagLayout());
        JFreeChart jc = ChartFactory.createPieChart("", createDataset(), true, false, false);
        PiePlot plot = (PiePlot) jc.getPlot();
        plot.setSectionPaint("Income", Color.GREEN);
        plot.setSectionPaint("Expense", Color.RED);

        ChartPanel chartPanel = new ChartPanel(jc);
        chartPanel.setPreferredSize(new Dimension(width, height));
        pnlChartTop.add(chartPanel);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        lbHeader = new javax.swing.JLabel();
        pnlChartTop = new javax.swing.JPanel();
        pnlSeller = new javax.swing.JPanel();
        pnlBestSeller = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblGoodSeller = new javax.swing.JTable();
        lbGoodSeller = new javax.swing.JLabel();
        pnlBadSeller = new javax.swing.JPanel();
        lbBadSeller = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblBadSeller = new javax.swing.JTable();
        pnlTexts = new javax.swing.JPanel();
        lbIncome = new javax.swing.JLabel();
        lbExpense = new javax.swing.JLabel();
        lbNet = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        lbHeader.setFont(new java.awt.Font("DB Adman X", 1, 24)); // NOI18N
        lbHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbHeader.setText("รายงานการเงิน");

        pnlChartTop.setBackground(new java.awt.Color(255, 204, 204));

        javax.swing.GroupLayout pnlChartTopLayout = new javax.swing.GroupLayout(pnlChartTop);
        pnlChartTop.setLayout(pnlChartTopLayout);
        pnlChartTopLayout.setHorizontalGroup(
            pnlChartTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 725, Short.MAX_VALUE)
        );
        pnlChartTopLayout.setVerticalGroup(
            pnlChartTopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 389, Short.MAX_VALUE)
        );

        pnlSeller.setLayout(new java.awt.GridLayout(2, 0));

        pnlBestSeller.setBackground(new java.awt.Color(135, 107, 82));

        tblGoodSeller.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblGoodSeller);

        lbGoodSeller.setForeground(new java.awt.Color(255, 255, 255));
        lbGoodSeller.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbGoodSeller.setText("Good Sellers (amount 10+)");

        javax.swing.GroupLayout pnlBestSellerLayout = new javax.swing.GroupLayout(pnlBestSeller);
        pnlBestSeller.setLayout(pnlBestSellerLayout);
        pnlBestSellerLayout.setHorizontalGroup(
            pnlBestSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBestSellerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbGoodSeller, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnlBestSellerLayout.setVerticalGroup(
            pnlBestSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBestSellerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbGoodSeller, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlSeller.add(pnlBestSeller);

        pnlBadSeller.setBackground(new java.awt.Color(111, 78, 51));

        lbBadSeller.setForeground(new java.awt.Color(255, 255, 255));
        lbBadSeller.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbBadSeller.setText("Bad Sellers (amount < 10)");

        tblBadSeller.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblBadSeller);

        javax.swing.GroupLayout pnlBadSellerLayout = new javax.swing.GroupLayout(pnlBadSeller);
        pnlBadSeller.setLayout(pnlBadSellerLayout);
        pnlBadSellerLayout.setHorizontalGroup(
            pnlBadSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbBadSeller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        pnlBadSellerLayout.setVerticalGroup(
            pnlBadSellerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBadSellerLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lbBadSeller, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))
        );

        pnlSeller.add(pnlBadSeller);

        pnlTexts.setLayout(new java.awt.GridLayout(1, 3, 6, 0));

        lbIncome.setText("income");
        pnlTexts.add(lbIncome);

        lbExpense.setText("expense");
        pnlTexts.add(lbExpense);

        lbNet.setText("net");
        pnlTexts.add(lbNet);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(307, 307, 307)
                        .addComponent(lbHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                        .addGap(340, 340, 340))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlTexts, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlChartTop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(pnlSeller, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlSeller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnlChartTop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnlTexts, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lbBadSeller;
    private javax.swing.JLabel lbExpense;
    private javax.swing.JLabel lbGoodSeller;
    private javax.swing.JLabel lbHeader;
    private javax.swing.JLabel lbIncome;
    private javax.swing.JLabel lbNet;
    private javax.swing.JPanel pnlBadSeller;
    private javax.swing.JPanel pnlBestSeller;
    private javax.swing.JPanel pnlChartTop;
    private javax.swing.JPanel pnlSeller;
    private javax.swing.JPanel pnlTexts;
    private javax.swing.JTable tblBadSeller;
    private javax.swing.JTable tblGoodSeller;
    // End of variables declaration//GEN-END:variables

    private void initText() {
        String txtIncome = "Total Income: " + income + " ฿";
        String txtExpense = "Total Expense: " + expense + " ฿";
        String txtNet;
        if (net > 0) {
            txtNet = "<html><nobr>Total Net: <font style='color:green'>" + net + " ฿</font></html>";
        } else {
            txtNet = "<html><nobr>Total Net: <font style='color:red'>" + net + " ฿</font></html>";
        }
        lbIncome.setText(txtIncome);
        lbExpense.setText(txtExpense);
        lbNet.setText(txtNet);
        lbHeader.setFont(CommonFont.getFontTitle());
        tblGoodSeller.setFont(CommonFont.getFont());
        tblBadSeller.setFont(CommonFont.getFont());
    }

    private void initTables() {
        String[] headTbl = {"Product Name", "Amount"};
        tblGoodSeller.setModel(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                int row = listGoodSold.size();
                if (row == 0) {
                    return 0;
                }
                return row;
            }

            @Override
            public int getColumnCount() {
                return headTbl.length;
            }

            @Override
            public String getColumnName(int column) {
                return headTbl[column];
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Product product;
                int amount;
                if (rowIndex < listGoodSold.size()) {
                    product = listGoodSold.keySet().toArray(new Product[0])[rowIndex];
                    amount = listGoodSold.get(product);
                    switch (columnIndex) {
                        case 0 -> {
                            return product.getName();
                        }
                        case 1 -> {
                            return amount;
                        }
                        default ->
                            throw new AssertionError();
                    }
                }
                return null;
            }
        });
        tblBadSeller.setModel(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                int row = listBadSold.size();
                if (row == 0) {
                    return 0;
                }
                return row;
            }

            @Override
            public int getColumnCount() {
                return headTbl.length;
            }

            @Override
            public String getColumnName(int column) {
                return headTbl[column];
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Product product;
                int amount;
                if (rowIndex < listBadSold.size()) {
                    product = listBadSold.keySet().toArray(new Product[0])[rowIndex];
                    amount = listBadSold.get(product);

                    switch (columnIndex) {
                        case 0 -> {
                            return product.getName();
                        }
                        case 1 -> {
                            return amount;
                        }
                        default ->
                            throw new AssertionError();
                    }
                }
                return null;
            }
        }
        );

    }
}
