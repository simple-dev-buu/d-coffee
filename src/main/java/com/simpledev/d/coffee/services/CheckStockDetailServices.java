/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CheckStockDetailDao;
import com.simpledev.d.coffee.models.CheckStockDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckStockDetailServices {
    public ArrayList<CheckStockDetail> getAll() {
        CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        return (ArrayList<CheckStockDetail>) chStockDetailDao.getAll();
    }
    public List<CheckStockDetail> getCheckStockDetailByOrder(String where, String order) {
        CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        return chStockDetailDao.getAll(where, order);
    }
    
     public List<CheckStockDetail> getCheckStockDetailByOrderDESC() {
        CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        return chStockDetailDao.getAll("checkStockDetail_id", "DESC");
    }
    
    public void addNew(CheckStockDetail editedStockDetail) {
       CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        chStockDetailDao.insert(editedStockDetail);
    }
    
    public void update(CheckStockDetail editedStockDetail) {
        CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        chStockDetailDao.update(editedStockDetail);
    }
    
    public void delete(CheckStockDetail editedStockDetail) {
        CheckStockDetailDao chStockDetailDao = new CheckStockDetailDao();
        chStockDetailDao.delete(editedStockDetail);
    }
}
